package com.github.alvarosct02.mymoviedb.features.main

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.navigateUp
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.github.alvarosct02.mymoviedb.BuildConfig
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.features.auth.AuthActivity
import com.github.alvarosct02.mymoviedb.features.base.BaseActivity
import com.github.alvarosct02.mymoviedb.utils.DialogManager
import com.github.alvarosct02.mymoviedb.utils.hideKeyboard
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class MainActivity : BaseActivity(), AppBarConfiguration.OnNavigateUpListener, MainView {

    private lateinit var appBarConfig: AppBarConfiguration
    private lateinit var presenter: MainPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupVariables()
        setupNavigation()
        presenter.init()
    }

    private fun setupVariables() {
        presenter = MainPresenter(this)
    }

    private fun setupNavigation() {

        setSupportActionBar(ly_toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

        val navController = findNavController(R.id.nav_host_fragment)
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            toolbar_title.hideKeyboard()
            toolbar_title.text = destination.label.toString()
        }

        appBarConfig = AppBarConfiguration(
                setOf(R.id.nav_home),
                drawer_layout
        )
        setupActionBarWithNavController(navController, appBarConfig)

        ly_toolbar.navigationIcon = ContextCompat.getDrawable(this, R.drawable.icon_empty)

        val menuRes = if (BuildConfig.DEBUG) R.menu.drawer_main_debug else R.menu.drawer_main
        navigationView.inflateMenu(menuRes)
        navigationView.setupWithNavController(navController)

        navigationView.setNavigationItemSelectedListener { menuItem ->
            menuItem.isChecked = true
            drawer_layout.closeDrawers()

            val id = menuItem.itemId
            presenter.handleMenuNavigation(id)
        }

        if (BuildConfig.DEBUG) {
            tv_build_number.visibility = View.VISIBLE
            tv_build_number.text = "${BuildConfig.FLAVOR.toUpperCase()} v${BuildConfig.VERSION_NAME}"
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            item.onNavDestinationSelected(findNavController(R.id.nav_host_fragment))
                    || super.onOptionsItemSelected(item)


    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navigateUp(findNavController(R.id.nav_host_fragment), appBarConfig)
    }

    override fun goToHome() {
        finish()
        startActivity(Intent(this, AuthActivity::class.java))
    }

    override fun displayErrorMessage(message: String) {
        DialogManager.showMessage(this, message)
    }

    override fun navigateToMenuItem(id: Int) {
        findNavController(R.id.nav_host_fragment).navigate(id)
    }
}
