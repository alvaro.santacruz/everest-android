package com.github.alvarosct02.mymoviedb.features.sync

import com.github.alvarosct02.mymoviedb.features.base.LoadingView

interface SyncView: LoadingView {

    fun onDownloadSuccess()

    fun onDownloadFails(error: String?)

    fun onUploadSuccess()

    fun onUploadFails(error: String?)

}
