package com.github.alvarosct02.mymoviedb.features.movies.list

import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.utils.adt.BinarySearchTree
import com.github.alvarosct02.mymoviedb.utils.toBitmap
import kotlinx.android.synthetic.main.item_movie.view.*


class MovieAdapter(
        movieTree: BinarySearchTree<Movie>,
        private val callback: (Movie, List<View>) -> Unit
) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    private val movies = movieTree.inOrderTraversal()

    lateinit var errorView: Drawable

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        val obj = movies[pos]

        ViewCompat.setTransitionName(holder.ivPhoto, obj.uniqueId())

        Glide.with(holder.ivPhoto.context)
                .load(obj.poster)
                .placeholder(R.color.browser_actions_bg_grey)
                .error(errorView)
                .centerCrop()
                .into(holder.ivPhoto)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_movie, parent, false)).apply {
            itemView.setOnClickListener {
                callback(movies[adapterPosition], listOf(ivPhoto))
            }

            val resources = parent.context.resources
            val bitmap = inflater.toBitmap(R.layout.layout_image_error,
                    width = resources.getDimensionPixelSize(R.dimen.movie_list_item_width),
                    height = resources.getDimensionPixelSize(R.dimen.movie_list_item_height))
            errorView = BitmapDrawable(resources, bitmap)

        }
    }

    override fun getItemCount() = movies.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivPhoto = view.iv_photo
    }
}