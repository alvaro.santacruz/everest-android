package com.github.alvarosct02.mymoviedb.features.main

import com.crashlytics.android.Crashlytics
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.DataSourceRepository
import com.github.alvarosct02.mymoviedb.features.base.BasePresenter
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class MainPresenter(
        view: MainView,
        private val auth: FirebaseAuth = FirebaseAuth.getInstance(),
        private val dataSourceRepository: DataSource = DataSourceRepository()
) : BasePresenter<MainView>(view) {

    fun init(){
        runCatching { Crashlytics.setUserIdentifier(FirebaseAuth.getInstance().currentUser?.uid) }
    }

    fun logout() {
        auth.signOut()
        disposable.add(dataSourceRepository.logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.goToHome()
                }, {
                    view.displayErrorMessage(it.message!!)
                }))
    }

    fun handleMenuNavigation(id: Int):Boolean {
        when (id) {
            R.id.logout -> {
                logout()
            }
            R.id.force_crash -> {
                throw RuntimeException("Forced Crash. To test Crashlytics :)")
            }
            else -> view.navigateToMenuItem(id)
        }
        return false
    }

}
