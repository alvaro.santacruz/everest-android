package com.github.alvarosct02.mymoviedb.features.movies.list;

public class Test {

    private String variable ;
    private String variable2 ;

    public static class Builder {

        private String variable ;
        private String variable2 ;

        public Builder(){

        }

        public Builder setVariable1(String valor) {
            variable = valor;
            return this;
        }

        public Builder setVariable2(String valor) {
            variable2 = valor;
            return this;
        }

        public Test build() {
             Test test = new Test();
            test.variable = this.variable;
            test.variable2 = this.variable2;
            return test;
        }

    }

    public static class Memento {
        public Memento current;


        public String variable ;
        public String variable2 ;


        public Memento getState() {
            return this.current;
        }

        public void setState(Memento newState) {
            this.current = newState;
        }

        public void saveState() {
            this.current = this;
        }

        public Memento undo() {
            return current;
        }

    }




    public void testBuilder() {
        Test test1 = new Builder()
                .setVariable1("dsada")
                .build();

        Test test2 = new Builder()
                .setVariable1("dsada")
                .setVariable2("dsada")
                .build();

        Test test3 = new Builder()
                .setVariable2("dsada")
                .build();


        Memento imple1 = new Memento();
        imple1.variable = "12313";
        imple1.variable2 = "2342432";

        imple1.saveState();

        imple1.variable = "3232";
        imple1.variable2 = "23232";

        imple1.undo();





    }
}
