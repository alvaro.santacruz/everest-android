package com.github.alvarosct02.mymoviedb.utils.adt

import org.jetbrains.annotations.TestOnly
import android.os.Build.VERSION_CODES.O
import android.icu.lang.UCharacter.GraphemeClusterBreak.T



open class DoublyLinkedList<T> : Iterable<T> {

    private var head: Node<T>? = null

    private data class Node<T>(
            val value: T,
            var prev: Node<T>?,
            var next: Node<T>?
    )

    fun addFirst(value: T) {
        val temp = Node(value, null, head)
        head?.prev = temp
        head = temp
    }

    fun removeFirst() {
        head = head?.next
        head?.prev = null
    }

    fun getLast(): T? {
        return getLastNode()?.value
    }

    fun getItemAt(position: Int): T? {
        var visitedNodes = 0
        var current = head
        while (current != null) {
            if (visitedNodes == position) return current.value
            visitedNodes += 1
            current = current.next
        }
        return null
    }

    fun addLast(value: T) {
        val last = getLastNode()
        val temp = Node(value, last, null)
        if (last != null) {
            last.next = temp
        } else {
            head = temp
        }
    }

    fun removeLast() {
        val last = getLastNode()
        if (last == head) {
            head = null
        } else {
            last?.prev?.next = null
        }
    }

    fun size(): Int {
        var count = 0
        var current = head
        while (current != null) {
            count += 1
            current = current.next
        }
        return count
    }

    private fun getLastNode(): Node<T>? {
        var current = head
        while (current?.next != null) {
            current = current.next
        }
        return current
    }

    @TestOnly
    fun convertToString(): String {
        var current = head
        val stringBuffer = StringBuffer()
        stringBuffer.append("|")
        while (current != null) {
            stringBuffer.append(current.value.toString())
            stringBuffer.append("|")
            current = current.next
        }
        return stringBuffer.toString()
    }

    override fun iterator(): Iterator<T> {
        return object : Iterator<T> {
            var currentNode: Node<T>? = head

            override fun hasNext(): Boolean {
                return currentNode != null
            }

            override fun next(): T {
                if (hasNext()) {
                    val value = currentNode!!.value
                    currentNode = currentNode?.next
                    return value
                } else {
                    throw NoSuchElementException()
                }
            }
        }
    }

}

