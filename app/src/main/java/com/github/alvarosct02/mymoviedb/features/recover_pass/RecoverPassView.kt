package com.github.alvarosct02.mymoviedb.features.recover_pass

import androidx.annotation.StringRes
import com.github.alvarosct02.mymoviedb.features.base.LoadingView

interface RecoverPassView: LoadingView {

    fun onRecoveryFails(message: String?)

    fun onEmailSent()

    fun showValidationError(message: String)

    fun showValidationError(@StringRes message: Int)

}
