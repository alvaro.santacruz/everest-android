package com.github.alvarosct02.mymoviedb.data.source.remote

import com.github.alvarosct02.mymoviedb.BuildConfig
import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query


object FirestoreManager {

    const val USERS = "users"
    const val MOVIES = "movies"
    const val ENVS = "envs"

    private fun getBaseRef() =FirebaseFirestore.getInstance()
            .collection(ENVS)
            .document(BuildConfig.FIREBASE_ENV)


    fun getMoviesByUser(userId: String): Query {
        return getBaseRef()
                .collection(USERS)
                .document(userId)
                .collection(MOVIES)
    }

    fun addMoviesOfUser(userId: String, movies: List<Movie>): Task<Void> {
        val batch = FirebaseFirestore.getInstance().batch()
        val moviesRef = getBaseRef()
                .collection(USERS)
                .document(userId)
                .collection(MOVIES)

        movies.forEach {
            moviesRef.document(it.uniqueId()).set(it)
        }

        return batch.commit()
    }


    fun deleteMoviesOfUser(userId: String, movies: List<Movie>): Task<Void> {
        val batch = FirebaseFirestore.getInstance().batch()
        val moviesRef = getBaseRef()
                .collection(USERS)
                .document(userId)
                .collection(MOVIES)

        movies.forEach {
            moviesRef.document(it.uniqueId()).delete()
        }

        return batch.commit()
    }

}