package com.github.alvarosct02.mymoviedb.features.home

import com.github.alvarosct02.mymoviedb.features.base.LoadingView

interface HomeView: LoadingView {

    fun onLoginFails(message: String?)

    fun onLoginSuccess()

}
