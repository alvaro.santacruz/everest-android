package com.github.alvarosct02.mymoviedb.utils

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.Toast
import androidx.annotation.StringRes
import com.github.alvarosct02.mymoviedb.R


object DialogManager {

    private var progressDialog: Dialog? = null
    private lateinit var toast: Toast

    fun showProgress(context: Context) {
        if (progressDialog != null) return
        progressDialog = Dialog(context).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
            setCancelable(false)
            setContentView(R.layout.dialog_progress)
            show()
        }
    }

    fun hideProgress() {
        progressDialog?.dismiss()
        progressDialog?.hide()
        progressDialog = null
    }

    fun showMessage(context: Context, @StringRes resString: Int) {
        showMessage(context, context.getString(resString))
    }

    @SuppressLint("ShowToast")
    fun showMessage(context: Context, message: String) {
        if (::toast.isInitialized.not()) {
            toast = Toast.makeText(context, message, Toast.LENGTH_SHORT)
        }
        toast.setText(message)
        toast.show()
    }

}