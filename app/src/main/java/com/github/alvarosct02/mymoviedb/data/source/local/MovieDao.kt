package com.github.alvarosct02.mymoviedb.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.github.alvarosct02.mymoviedb.data.models.Movie
import io.reactivex.Single

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovie(movie: Movie)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllMovies(vararg movie: Movie): List<Long>

    @Query("SELECT * FROM Movie")
    fun getAllMovies(): Single<List<Movie>>

    @Query("SELECT * FROM Movie WHERE imdbID = :id")
    fun getMovieById(id: String): Single<Movie>

    @Query("DELETE FROM Movie WHERE imdbID = :id AND keywordUsed = :keyword ")
    fun deleteById(id: String, keyword: String)

    @Query("DELETE FROM Movie")
    fun deleteAll()

}
