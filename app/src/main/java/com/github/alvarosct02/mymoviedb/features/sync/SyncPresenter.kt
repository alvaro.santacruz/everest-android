package com.github.alvarosct02.mymoviedb.features.sync

import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.DataSourceRepository
import com.github.alvarosct02.mymoviedb.features.base.BasePresenter
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class SyncPresenter(
        view: SyncView,
        private val dataRepo: DataSource = DataSourceRepository()
) : BasePresenter<SyncView>(view) {

    fun uploadMovies() {
        view.showLoading()

        disposable.add(dataRepo.getSavedMovies().flatMapCompletable { movies ->

            val moviesToBeStored = movies.filter { it.isSaved }
            val moviesToBeDeleted = movies.filter { it.isSaved.not() }

            Completable.merge(listOf(
                    dataRepo.addMoviesOfCurrentUser(moviesToBeStored),
                    dataRepo.deleteMoviesOfCurrentUser(moviesToBeDeleted)
                            .andThen(Completable.merge(moviesToBeDeleted.map { dataRepo.deleteMovieLocally(it) }))
            ))
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideLoading()
                    view.onUploadSuccess()
                }, {
                    view.hideLoading()
                    view.onUploadFails(it.message)
                })
        )
    }

    fun downloadMovies() {
        view.showLoading()
        disposable.add(dataRepo.getMyMoviesFromCloud()
                .flatMapCompletable {
                    dataRepo.saveMoviesLocally(it)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideLoading()
                    view.onDownloadSuccess()
                }, {
                    view.hideLoading()
                    view.onDownloadFails(it.message)
                })
        )
    }

}
