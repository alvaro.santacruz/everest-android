package com.github.alvarosct02.mymoviedb.utils.adt

open class BinarySearchTree<T>(
    val comparator: (T, T) -> Int
) {
    private var root: Node<T>? = null

    private data class Node<T>(
        val value: T,
        var left: Node<T>?,
        var right: Node<T>?
    ) {

        fun hasLeftChild() = left != null

        fun hasRightChild() = right != null

        fun isLeaf() = !hasLeftChild() && !hasRightChild()
    }

    fun add(value: T) {
        val temp = Node(value, null, null)
        if (root == null) {
            root = temp
        } else {
            var currentNode = root
            while (currentNode != null) {
                val result = comparator(temp.value, currentNode.value)

                if (result <= 0 && currentNode.hasLeftChild()) {
                    currentNode = currentNode.left
                } else if (result <= 0) {
                    currentNode.left = temp
                    return
                } else if (result > 0 && currentNode.hasRightChild()) {
                    currentNode = currentNode.right
                } else if (result > 0) {
                    currentNode.right = temp
                    return
                }
            }
        }
    }

    fun remove(value: T) {

//        Empty Tree Case
        if (root == null) return

        var foundInLeft = false
        var foundInRight = false
        val foundInRoot = root?.value == value

        var currentNode = root
        while (!foundInRoot && currentNode != null) {

            val result = comparator(value, currentNode.value)

            if (result <= 0 && currentNode.hasLeftChild()) {
                if (currentNode.left?.value == value) {
                    foundInLeft = true
                    break
                }
                currentNode = currentNode.left
            } else if (currentNode.hasRightChild()) {
                if (currentNode.right?.value == value) {
                    foundInRight = true
                    break
                }
                currentNode = currentNode.right
            } else {
                break
            }
        }

        val parentNode = currentNode!!
        val nodeToDelete = when {
            foundInRoot -> root
            foundInLeft -> parentNode.left
            foundInRight -> parentNode.right
            else -> return
        }!!

        val nodeToReplace = when {
            nodeToDelete.isLeaf() -> null
            nodeToDelete.hasLeftChild() && !nodeToDelete.hasRightChild() -> nodeToDelete.left
            !nodeToDelete.hasLeftChild() && nodeToDelete.hasRightChild() -> nodeToDelete.right
            else -> {  //  Both branches. Search for min node in the right branch to replace the deleted one
                var parentMin: Node<T>? = nodeToDelete
                var currentMin = nodeToDelete.right!!
                while (currentMin.left != null) {
                    parentMin = currentMin
                    currentMin = currentMin.left!!
                }

                currentMin.left = nodeToDelete.left
                currentMin.right = nodeToDelete.right

                //  Prevent cycles
                if (parentMin == nodeToDelete) {
                    currentMin.right = null
                } else {
                    parentMin?.left = null
                }

                currentMin
            }
        }

        when {
            foundInRoot -> root = nodeToReplace
            foundInLeft -> parentNode.left = nodeToReplace
            foundInRight -> parentNode.right = nodeToReplace
        }

    }

    fun size() = size(root)

    fun preOrderTraversal() = preOrderTraversal(root)

    fun inOrderTraversal() = inOrderTraversal(root)

    fun postOrderTraversal() = postOrderTraversal(root)

    private fun size(node: Node<T>?) : Int {
        if (node == null) return 0
        return 1 + size(node.left) + size(node.right)
    }


    private fun preOrderTraversal(node: Node<T>?): List<T> {
        if (node == null) return listOf()
        return mutableListOf<T>().apply {
            add(node.value)
            addAll(preOrderTraversal(node.left))
            addAll(preOrderTraversal(node.right))
        }
    }

    private fun inOrderTraversal(node: Node<T>?): List<T> {
        if (node == null) return listOf()
        return mutableListOf<T>().apply {
            addAll(preOrderTraversal(node.left))
            add(node.value)
            addAll(preOrderTraversal(node.right))
        }
    }

    private fun postOrderTraversal(node: Node<T>?): List<T> {
        if (node == null) return listOf()
        return mutableListOf<T>().apply {
            addAll(preOrderTraversal(node.left))
            addAll(preOrderTraversal(node.right))
            add(node.value)
        }
    }

}
