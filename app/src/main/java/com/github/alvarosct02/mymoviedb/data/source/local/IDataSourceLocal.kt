package com.github.alvarosct02.mymoviedb.data.source.local

import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.utils.adt.DoublyLinkedList
import io.reactivex.Completable
import io.reactivex.Single

interface IDataSourceLocal {

    fun saveMovies(movie: List<Movie>): Completable

    fun saveMovie(movie: Movie): Completable

    fun updateMovie(movie: Movie): Completable

    fun forgetMovie(movie: Movie): Completable

    fun resetKeywords()

    fun saveKeywords(keywords: List<String>)

    fun saveKeywords(keyword: String)

    fun getKeywords(): DoublyLinkedList<String>

    fun getSavedMovies(): Single<List<Movie>>

    fun getSavedMovie(id: String): Single<Movie>

    fun logout(): Completable

}