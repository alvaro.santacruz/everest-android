package com.github.alvarosct02.mymoviedb.features.movies.add

import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.DataSourceRepository
import com.github.alvarosct02.mymoviedb.features.base.BasePresenter

open class MovieAddPresenter(
        view: MovieAddView,
        private val dataSourceRepository: DataSource = DataSourceRepository()
) : BasePresenter<MovieAddView>(view) {

    fun searchMovies(keywords: List<String>) {
        val keywordsClean = keywords.map { it.trim() }
        when {
            keywordsClean.contains("") -> {
                view.showEmptyFieldError()
            }
            keywordsClean.groupingBy { it }.eachCount().filter { it.value > 1 }.isNotEmpty() -> {
//            If contains duplicates
                view.showDuplicateFieldError()
            }
            else -> {
                dataSourceRepository.saveKeywords(keywords)
                view.onSuccess()
            }
        }

    }
}
