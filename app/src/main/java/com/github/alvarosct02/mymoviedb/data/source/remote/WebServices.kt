package com.github.alvarosct02.mymoviedb.data.source.remote

import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.data.pojos.SearchMovieResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface WebServices {

    @GET(".")
    fun search(
        @Query("apikey") apiKey: String,
        @Query("s") term: String
    ): Observable<SearchMovieResponse>

    @GET(".")
    fun getMovie(
        @Query("apikey") apiKey: String,
        @Query("i") id: String
    ): Observable<Movie>
}
