package com.github.alvarosct02.mymoviedb.features.login

import androidx.annotation.StringRes
import com.github.alvarosct02.mymoviedb.features.base.LoadingView

interface LoginView: LoadingView {

    fun onLoginFails(message: String?)

    fun onLoginSuccess()

    fun showValidationError(message: String)

    fun showValidationError(@StringRes message: Int)

}
