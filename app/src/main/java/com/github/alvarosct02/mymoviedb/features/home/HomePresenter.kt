package com.github.alvarosct02.mymoviedb.features.home

class HomePresenter(
        private val view: HomeView,
        private val facebookLoginInteractor: FacebookLoginInteractor
) {

    fun attemptLogin() {
        facebookLoginInteractor.login(
                onSuccess = view::onLoginSuccess,
                onFailure = view::onLoginFails
        )
    }
}
