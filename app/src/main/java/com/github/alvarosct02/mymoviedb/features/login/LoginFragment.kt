package com.github.alvarosct02.mymoviedb.features.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.features.base.BaseFragment
import com.github.alvarosct02.mymoviedb.features.main.MainActivity
import com.github.alvarosct02.mymoviedb.features.recover_pass.RecoverPassFragmentDialog
import com.github.alvarosct02.mymoviedb.utils.DialogManager
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment : BaseFragment(), LoginView {

    private lateinit var presenter: LoginPresenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun setupVariables() {
        super.setupVariables()
        presenter = LoginPresenter(this)
    }

    override fun setupViews() {
        super.setupViews()

        bt_login.setOnClickListener {
            presenter.loginUser(
                email = til_email.editText?.text.toString(),
                password = til_password.editText?.text.toString()
            )
        }

        bt_sign_up.setOnClickListener {
            findNavController().navigate(R.id.nav_sign_up)
        }

        bt_recover_password.setOnClickListener {
            RecoverPassFragmentDialog().show(childFragmentManager, "fragment_alert")
        }

    }

    override fun onLoginFails(message: String?) {
        DialogManager.showMessage(
            requireContext(), message
                ?: getString(R.string.message_error_default)
        )
    }

    override fun onLoginSuccess() {
        startActivity(Intent(requireContext(), MainActivity::class.java))
        activity?.finish()
        hideLoading()
    }

    override fun showValidationError(message: String) {
        DialogManager.showMessage(requireContext(), message)
    }

    override fun showValidationError(message: Int) {
        DialogManager.showMessage(requireContext(), message)
    }

}
