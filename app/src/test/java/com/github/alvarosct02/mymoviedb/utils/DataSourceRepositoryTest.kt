package com.github.alvarosct02.mymoviedb.utils

import com.github.alvarosct02.mymoviedb.data.source.DataSourceRepository
import com.github.alvarosct02.mymoviedb.data.source.local.DataSourceLocal
import com.github.alvarosct02.mymoviedb.data.source.local.IDataSourceLocal
import com.github.alvarosct02.mymoviedb.data.source.remote.DataSourceRemote
import com.github.alvarosct02.mymoviedb.data.source.remote.IDataSourceRemote

open class DataSourceRepositoryTest(
        val fixedUserId:String,
        remote: IDataSourceRemote = DataSourceRemote(),
        local: IDataSourceLocal = DataSourceLocal()
) : DataSourceRepository(remote, local) {

    override fun getUserId(): String {
        return fixedUserId
    }
}
