package com.github.alvarosct02.mymoviedb.features.movies.add

interface MovieAddView {

    fun onSuccess()

    fun showEmptyFieldError()

    fun showDuplicateFieldError()

}
