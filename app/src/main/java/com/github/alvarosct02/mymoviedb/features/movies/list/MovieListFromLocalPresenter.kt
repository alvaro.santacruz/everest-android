package com.github.alvarosct02.mymoviedb.features.movies.list

import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.DataSourceRepository
import com.github.alvarosct02.mymoviedb.utils.adt.BinarySearchTree
import com.github.alvarosct02.mymoviedb.utils.adt.DoublyLinkedList
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class MovieListFromLocalPresenter(
        view: MovieListView,
        private val dataSourceRepository: DataSource = DataSourceRepository(),
        private val movieList: DoublyLinkedList<Pair<String, BinarySearchTree<Movie>>> = DoublyLinkedList()
) : MovieListPresenter(view, dataSourceRepository, movieList) {

    override fun searchMovies() {
        view.showLoading()
        disposable.add(dataSourceRepository.getSavedMovies()
                .toObservable()
                .flatMapIterable { it }
                .map { storeMovieInAdt(it) }
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideLoading()
                    if (it.isEmpty()) view.displayNoMoviesMessage() else showMovies(true)
                }, {
                    view.hideLoading()
                    view.displayNoMoviesMessage()
                })
        )
    }
}
