package com.github.alvarosct02.mymoviedb.features.movies.detail

import com.github.alvarosct02.mymoviedb.features.base.LoadingView

interface MovieDetailView: LoadingView {

    fun displayTitle(title: String)

    fun displayPhoto(photoUri: String)

    fun displayReleaseDate(releaseDate: String)

    fun displayPlot(plot: String)

    fun displayRating(stars: Double, reviewCount: String)

//    ... Many others

    fun displaySavedIndicator(isSaved: Boolean)

}
