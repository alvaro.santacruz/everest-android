package com.github.alvarosct02.mymoviedb.features.recover_pass

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.features.base.BaseDialogFragment
import com.github.alvarosct02.mymoviedb.utils.DialogManager
import kotlinx.android.synthetic.main.fragment_recover_pass.*

class RecoverPassFragmentDialog : BaseDialogFragment(), RecoverPassView {

    private lateinit var presenter: RecoverPassPresenter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recover_pass, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
        }
    }

    override fun setupVariables() {
        super.setupVariables()
        presenter = RecoverPassPresenter(this)
    }

    override fun setupViews() {
        super.setupViews()

        bt_send.setOnClickListener {
            presenter.requestPasswordReset(til_email.editText?.text.toString())
        }

    }

    override fun onRecoveryFails(message: String?) {
        DialogManager.showMessage(requireContext(), message
                ?: getString(R.string.message_error_default))
    }

    override fun onEmailSent() {
        dialog?.hide()
        DialogManager.showMessage(requireContext(), getString(R.string.recover_pass_success))
    }

    override fun showValidationError(message: String) {
        DialogManager.showMessage(requireContext(), message)
    }

    override fun showValidationError(message: Int) {
        DialogManager.showMessage(requireContext(), message)
    }

}
