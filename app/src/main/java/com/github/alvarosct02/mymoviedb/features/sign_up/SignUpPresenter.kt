package com.github.alvarosct02.mymoviedb.features.sign_up

import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.DataSourceRepository
import com.github.alvarosct02.mymoviedb.features.base.BasePresenter
import com.github.alvarosct02.mymoviedb.utils.isValidEmail
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class SignUpPresenter(
    view: SignUpView,
    private val dataRepo: DataSource = DataSourceRepository()
) : BasePresenter<SignUpView>(view) {

    fun signUpUser(name: String, email: String, password: String, passwordConfirmation: String) {

        if (!areAllFieldsValid(name, email, password, passwordConfirmation)) return

        view.showLoading()

        disposable.add(
            dataRepo.signUpUserWithEmail(name, email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideLoading()
                    view.onSignUpSuccess()
                }, {
                    view.hideLoading()
                    view.onSignUpFails(it.message)
                })
        )
    }

    private fun areAllFieldsValid(
        name: String,
        email: String,
        password: String,
        passwordConfirmation: String
    ): Boolean {
        when {
            name.isBlank() -> view.showValidationError(R.string.message_error_empty_fields)
            email.isBlank() -> view.showValidationError(R.string.message_error_empty_fields)
            password.isBlank() -> view.showValidationError(R.string.message_error_empty_fields)
            passwordConfirmation.isBlank() -> view.showValidationError(R.string.message_error_empty_fields)
            !isValidEmail(email) -> view.showValidationError(R.string.message_error_email_format)
            password.length < 8 -> view.showValidationError(R.string.message_error_password_length)
            password != passwordConfirmation -> view.showValidationError(R.string.message_error_password_confirmation)
            else -> return true
        }
        return false
    }

}
