package com.github.alvarosct02.mymoviedb.features.recover_pass

import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.DataSourceRepository
import com.github.alvarosct02.mymoviedb.features.base.BasePresenter
import com.github.alvarosct02.mymoviedb.utils.isValidEmail
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class RecoverPassPresenter(
    view: RecoverPassView,
    private val dataRepo: DataSource = DataSourceRepository()
) : BasePresenter<RecoverPassView>(view) {

    fun requestPasswordReset(email: String) {

        if (!areAllFieldsValid(email)) return

        view.showLoading()

        disposable.add(
            dataRepo.sendPasswordResetEmail(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideLoading()
                    view.onEmailSent()
                }, {
                    view.hideLoading()
                    view.onRecoveryFails(it.message)
                })
        )
    }

    private fun areAllFieldsValid(email: String): Boolean {
        when {
            email.isBlank() -> view.showValidationError(R.string.message_error_empty_fields)
            !isValidEmail(email) -> view.showValidationError(R.string.message_error_email_format)
            else -> return true
        }
        return false
    }

}
