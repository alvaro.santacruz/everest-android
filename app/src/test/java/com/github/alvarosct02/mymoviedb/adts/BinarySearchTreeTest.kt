package com.github.alvarosct02.mymoviedb.adts

import com.github.alvarosct02.mymoviedb.utils.adt.BinarySearchTree
import org.junit.Assert.assertEquals
import org.junit.Test

class BinarySearchTreeTest {

    @Test
    fun `test add on empty tree`() {
        val tree = BinarySearchTree<String> { v1, v2 -> v1.compareTo(v2) }
        tree.add("False")
        assertEquals(listOf("False"), tree.preOrderTraversal())
    }

    @Test
    fun `test add`() {
        val tree = BinarySearchTree<String> { v1, v2 -> v1.compareTo(v2) }
        tree.add("False")
        tree.add("Binary")
        tree.add("Dino")
        assertEquals(listOf("Binary", "Dino", "False"), tree.inOrderTraversal())
    }

    @Test
    fun `test preOrderTraversal`() {
        val tree = BinarySearchTree<Int> { v1, v2 -> v1.compareTo(v2) }
        tree.add(50)
        tree.add(25)
        tree.add(75)
        assertEquals(listOf(50, 25, 75), tree.preOrderTraversal())
    }

    @Test
    fun `test inOrderTraversal`() {
        val tree = BinarySearchTree<Int> { v1, v2 -> v1.compareTo(v2) }
        tree.add(50)
        tree.add(25)
        tree.add(75)
        assertEquals(listOf(25, 50, 75), tree.inOrderTraversal())
    }

    @Test
    fun `test postOrderTraversal`() {
        val tree = BinarySearchTree<Int> { v1, v2 -> v1.compareTo(v2) }
        tree.add(50)
        tree.add(25)
        tree.add(75)
        assertEquals(listOf(25, 75, 50), tree.postOrderTraversal())
    }

    @Test
    fun `test remove on empty tree`() {
        val tree = BinarySearchTree<Int> { v1, v2 -> v1.compareTo(v2) }
        tree.remove(4)
        assertEquals(listOf<Int>(), tree.inOrderTraversal())
    }

    @Test
    fun `test remove non-existing value in tree`() {
        val tree = BinarySearchTree<Int> { v1, v2 -> v1.compareTo(v2) }
        tree.add(4)
        tree.add(2)
        tree.add(3)
        tree.remove(6)
        assertEquals(listOf(2, 3, 4), tree.inOrderTraversal())
    }

    @Test
    fun `test remove only node in tree`() {
        val tree = BinarySearchTree<Int> { v1, v2 -> v1.compareTo(v2) }
        tree.add(4)
        tree.remove(4)
        assertEquals(listOf<Int>(), tree.postOrderTraversal())
    }

    @Test
    fun `test remove node with only one branch`() {
        val tree = BinarySearchTree<Int> { v1, v2 -> v1.compareTo(v2) }
        tree.add(50)
        tree.add(25)
        tree.add(75)
        tree.add(60)
        tree.add(64)
        tree.add(65)
        tree.add(62)
        /*
                        50
                 25                        75
                               60                   -
                            -       64
                                  62  65
        */
        assertEquals(listOf(50, 25, 75, 60, 64, 62, 65), tree.preOrderTraversal())

        tree.remove(75)
        /*
                        50
                 25             60                   -
                             -      64
                                  62  65
        */
        assertEquals(listOf(50, 25, 60, 64, 62, 65), tree.preOrderTraversal())

    }

    @Test
    fun `test remove leaf node `() {
        val tree = BinarySearchTree<Int> { v1, v2 -> v1.compareTo(v2) }
        tree.add(50)
        tree.add(25)
        tree.add(75)
        tree.add(60)
        tree.add(64)
        tree.add(65)
        tree.add(62)
        /*
                        50
                 25                        75
                               60                   -
                            -       64
                                  62  65
        */
        tree.remove(65)
        /*
                        50
                 25                        75
                               60                   -
                            -       64
                                  62  -
        */
        assertEquals(listOf(50, 25, 75, 60, 64, 62), tree.preOrderTraversal())
    }

    @Test
    fun `test remove node with two branches`() {
        val tree = BinarySearchTree<Int> { v1, v2 -> v1.compareTo(v2) }
        tree.add(50)
        tree.add(25)
        tree.add(75)
        tree.add(60)
        tree.add(64)
        tree.add(80)
        tree.add(89)
        tree.add(79)
        /*
                        50
                 25                  75
                              60            80
                                 64      79    89
        */

        tree.remove(75)
        /*
                        50
                 25                  79
                              60            80
                                 64            89
        */
        val preOrder = tree.preOrderTraversal()
        assertEquals(listOf(50, 25, 79, 60, 64, 80, 89), preOrder)
    }

    @Test
    fun `test remove root with two branches`() {
        val tree = BinarySearchTree<Int> { v1, v2 -> v1.compareTo(v2) }
        tree.add(50)
        tree.add(25)
        tree.add(75)
        /*
                    50
                 25    75
        */
        tree.remove(50)
        /*
                    75
                 25    -
        */
        assertEquals(75, tree.preOrderTraversal()[0])
    }

}
