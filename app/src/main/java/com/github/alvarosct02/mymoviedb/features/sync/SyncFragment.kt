package com.github.alvarosct02.mymoviedb.features.sync

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.features.base.BaseFragment
import com.github.alvarosct02.mymoviedb.utils.DialogManager
import kotlinx.android.synthetic.main.fragment_sync.*

class SyncFragment : BaseFragment(), SyncView {

    private lateinit var presenter: SyncPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sync, container, false)
    }

    override fun setupVariables() {
        super.setupVariables()
        presenter = SyncPresenter(this)
    }

    override fun setupViews() {
        super.setupViews()

        bt_download.setOnClickListener {
            presenter.downloadMovies()
        }

        bt_upload.setOnClickListener {
            presenter.uploadMovies()
        }
    }

    override fun onDownloadSuccess() {
        DialogManager.showMessage(requireContext(), getString(R.string.sync_message_download_success))
    }

    override fun onDownloadFails(error: String?) {
        DialogManager.showMessage(requireContext(), getString(R.string.sync_message_download_fails))
    }

    override fun onUploadSuccess() {
        DialogManager.showMessage(requireContext(), getString(R.string.sync_message_upload_success))
    }

    override fun onUploadFails(error: String?) {
        DialogManager.showMessage(requireContext(), getString(R.string.sync_message_upload_fails))
    }
}
