package com.github.alvarosct02.mymoviedb.adts

import com.github.alvarosct02.mymoviedb.utils.adt.DoublyLinkedList
import org.junit.Assert.assertEquals
import org.junit.Test

class DoublyLinkedListTest {

    @Test
    fun `test addFirst first time`() {
        val list = DoublyLinkedList<String>()
        list.addFirst("Doing")
        assertEquals("|Doing|", list.convertToString())
    }

    @Test
    fun `test addFirst multiple times`() {
        val list = DoublyLinkedList<String>()
        list.addFirst("Doing")
        list.addFirst("You")
        list.addFirst("How")
        list.addFirst("Hey")
        assertEquals("|Hey|How|You|Doing|", list.convertToString())
    }

    @Test
    fun `test removeFirst on empty list does not crash`() {
        val list = DoublyLinkedList<String>()
        list.removeFirst()
        assertEquals("|", list.convertToString())
    }

    @Test
    fun `test removeFirst`() {
        val list = DoublyLinkedList<String>()
        list.addFirst("Doing")
        list.addFirst("You")
        list.addFirst("How")
        list.addFirst("Hey")
        list.removeFirst()
        assertEquals("|How|You|Doing|", list.convertToString())
    }

    @Test
    fun `test getLast on empty list`() {
        val list = DoublyLinkedList<String>()
        assertEquals(null, list.getLast())
    }

    @Test
    fun `test getLast`() {
        val list = DoublyLinkedList<String>()
        list.addFirst("Doing")
        list.addFirst("You")
        list.addFirst("How")
        list.addFirst("Hey")
        assertEquals("Doing", list.getLast())
    }

    @Test
    fun `test addLast first time`() {
        val list = DoublyLinkedList<String>()
        list.addLast("Hey")
        assertEquals("|Hey|", list.convertToString())
    }

    @Test
    fun `test addLast multiple times`() {
        val list = DoublyLinkedList<String>()
        list.addLast("Hey")
        list.addLast("How")
        list.addLast("You")
        list.addLast("Doing")
        assertEquals("|Hey|How|You|Doing|", list.convertToString())
    }


    @Test
    fun `test removeLast on empty list does not crash`() {
        val list = DoublyLinkedList<String>()
        list.removeLast()
        assertEquals("|", list.convertToString())
    }

    @Test
    fun `test removeLast`() {
        val list = DoublyLinkedList<String>()
        list.addFirst("?")
        list.addFirst("Doing")
        list.addFirst("You")
        list.addFirst("How")
        list.removeLast()
        assertEquals("|How|You|Doing|", list.convertToString())
    }

    @Test
    fun `test size on empty list`() {
        val list = DoublyLinkedList<String>()
        assertEquals(0, list.size())
    }

    @Test
    fun `test size`() {
        val list = DoublyLinkedList<String>()
        list.addFirst("?")
        list.addFirst("Doing")
        list.addFirst("You")
        list.addFirst("How")
        assertEquals(4, list.size())
    }

    @Test
    fun `test getItemAt on empty list`() {
        val list = DoublyLinkedList<Int>()
        assertEquals(null, list.getItemAt(0))
    }

    @Test
    fun `test getItemAt out of range`() {
        val list = DoublyLinkedList<Int>()
        list.addFirst(3)
        list.addFirst(60)
        list.addFirst(9)
        list.addFirst(12)
        assertEquals(null, list.getItemAt(9))
    }

    @Test
    fun `test getItemAt`() {
        val list = DoublyLinkedList<Int>()
        list.addLast(3)
        list.addLast(60)
        list.addLast(9)
        list.addLast(12)
        assertEquals(9, list.getItemAt(2))
    }

    @Test
    fun `test all methods together`() {
        val list = DoublyLinkedList<String>()

        list.removeLast()
        list.removeFirst()

        list.addFirst("Doing")
        list.addFirst("You")
        list.addFirst("How")
        list.removeLast()
        list.addLast("Doing")
        list.addLast("?")
        list.removeFirst()
        list.addFirst("How")

        assertEquals("|How|You|Doing|?|", list.convertToString())
    }

    @Test
    fun `test iterable`() {
        val list = DoublyLinkedList<String>()

        list.removeLast()
        list.removeFirst()

        list.addFirst("?")
        list.addFirst("Doing")
        list.addFirst("You")
        list.addFirst("How")

        val buffer = StringBuffer()
        buffer.append("|")
        list.forEach {
            buffer.append(it)
            buffer.append("|")
        }

        assertEquals("|How|You|Doing|?|", buffer.toString())
    }
}
