package com.github.alvarosct02.mymoviedb.features.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.facebook.CallbackManager
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.features.base.BaseFragment
import com.github.alvarosct02.mymoviedb.features.main.MainActivity
import com.github.alvarosct02.mymoviedb.utils.DialogManager
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : BaseFragment(), HomeView {

    private val callbackManager = CallbackManager.Factory.create()
    private lateinit var presenter: HomePresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun setupVariables() {
        super.setupVariables()
        val loginInteractor = FacebookLoginInteractor(this, callbackManager)
        presenter = HomePresenter(this, loginInteractor)
    }

    override fun setupViews() {
        super.setupViews()

        bt_facebook_login.setOnClickListener {
            showLoading()
            presenter.attemptLogin()
        }

        bt_email_login.setOnClickListener {
            findNavController().navigate(R.id.nav_login)
        }
    }

    override fun onLoginSuccess() {
        startActivity(Intent(requireContext(), MainActivity::class.java))
        activity?.finish()
        hideLoading()
    }

    override fun onLoginFails(message: String?) {
        hideLoading()
        DialogManager.showMessage(requireContext(), message
                ?: getString(R.string.message_error_default))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }


}
