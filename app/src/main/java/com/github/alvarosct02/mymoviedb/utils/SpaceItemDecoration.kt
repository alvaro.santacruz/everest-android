package com.github.alvarosct02.mymoviedb.utils

import android.content.Context
import android.graphics.Rect
import android.util.TypedValue
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class SpaceItemDecoration(context: Context, inBetween: Int = 0, top: Int = 0,
                          bottom: Int = 0, left: Int = 0, right: Int = 0)
    : RecyclerView.ItemDecoration() {

    private val displayMetrics = context.resources.displayMetrics
    private val unit = TypedValue.COMPLEX_UNIT_DIP
    private val inBetweenSpace = TypedValue.applyDimension(unit, inBetween.toFloat(), displayMetrics).toInt()
    private val topSpace = TypedValue.applyDimension(unit, top.toFloat(), displayMetrics).toInt()
    private val bottomSpace = TypedValue.applyDimension(unit, bottom.toFloat(), displayMetrics).toInt()
    private val leftSpace = TypedValue.applyDimension(unit, left.toFloat(), displayMetrics).toInt()
    private val rightSpace = TypedValue.applyDimension(unit, right.toFloat(), displayMetrics).toInt()

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val orientation = (parent.layoutManager as LinearLayoutManager).orientation

        when (orientation) {
            LinearLayoutManager.VERTICAL -> getItemOffsetsForVertical(outRect, view, parent)
            LinearLayoutManager.HORIZONTAL -> getItemOffsetsForHorizontal(outRect, view, parent)
        }
    }

    private fun getItemOffsetsForVertical(outRect: Rect, view: View, parent: RecyclerView) {
        outRect.right = rightSpace
        outRect.left = leftSpace

        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = topSpace
        }

        if (parent.getChildAdapterPosition(view) != parent.adapter!!.itemCount - 1) {
            outRect.bottom = inBetweenSpace
        } else {
            outRect.bottom = bottomSpace
        }
    }


    private fun getItemOffsetsForHorizontal(outRect: Rect, view: View, parent: RecyclerView) {
        outRect.top = topSpace
        outRect.bottom = bottomSpace

        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.left = leftSpace
        }

        if (parent.getChildAdapterPosition(view) != parent.adapter!!.itemCount - 1) {
            outRect.right = inBetweenSpace
        } else {
            outRect.right = rightSpace
        }
    }

}
