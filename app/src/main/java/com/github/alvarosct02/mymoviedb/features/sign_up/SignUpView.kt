package com.github.alvarosct02.mymoviedb.features.sign_up

import androidx.annotation.StringRes
import com.github.alvarosct02.mymoviedb.features.base.LoadingView

interface SignUpView: LoadingView {

    fun onSignUpFails(message: String?)

    fun showValidationError(message: String)

    fun showValidationError(@StringRes message: Int)

    fun onSignUpSuccess()

}
