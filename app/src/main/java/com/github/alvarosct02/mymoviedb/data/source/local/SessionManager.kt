package com.github.alvarosct02.mymoviedb.data.source.local

import android.content.Context
import android.content.SharedPreferences
import com.github.alvarosct02.mymoviedb.BuildConfig
import com.github.alvarosct02.mymoviedb.utils.Constants

object SessionManager {

    private var prefs: SharedPreferences? = null

    fun init(context: Context) {
        prefs = context.getSharedPreferences(BuildConfig.PREFERENCES_NAME, Context.MODE_PRIVATE)
    }

    fun resetKeywordList() {
        prefs?.edit()?.apply {
            putStringSet(Constants.PREFS_KEYWORDS, HashSet())
            apply()
        }
    }

    fun addToKeywordList(keywords: List<String>) {
        val mergedList = getKeywordList().union(keywords)
        prefs?.edit()?.apply {
            putStringSet(Constants.PREFS_KEYWORDS, HashSet(mergedList))
            apply()
        }
    }

    fun getKeywordList(): List<String> {
        return prefs?.getStringSet(Constants.PREFS_KEYWORDS, null)?.toList() ?: listOf()
    }

}