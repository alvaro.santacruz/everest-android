package com.github.alvarosct02.mymoviedb.features.movies.detail

import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.BounceInterpolator
import android.view.animation.ScaleAnimation
import com.bumptech.glide.Glide
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.features.base.BaseActivity
import com.github.alvarosct02.mymoviedb.utils.Constants
import com.github.alvarosct02.mymoviedb.utils.DialogManager
import com.github.alvarosct02.mymoviedb.utils.toBitmap
import kotlinx.android.synthetic.main.activity_movie_detail.*


class MovieDetailActivity : BaseActivity(), MovieDetailView {

    private lateinit var movieId: String
    private lateinit var movieKeyword: String
    private var loadedFromLocal: Boolean = false
    private lateinit var presenter: MovieDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            iv_image.transitionName = intent?.getStringExtra(Constants.EXTRA_MOVIE_IMAGE_TRANSITION_ID)
        }

        setupVariables()
        setupViews()

        presenter.getMovie(movieId, movieKeyword)
    }

    private fun setupVariables() {
        movieId = intent?.getStringExtra(Constants.EXTRA_MOVIE_ID) ?: ""
        movieKeyword = intent?.getStringExtra(Constants.EXTRA_MOVIE_KEYWORD) ?: ""
        loadedFromLocal = intent?.getBooleanExtra(Constants.EXTRA_LOAD_FROM_LOCAL, false)?: false
        presenter = MovieDetailPresenter(this)
    }

    private fun setupViews() {
        val scaleAnimation = ScaleAnimation(0.7f, 1.0f, 0.7f, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        scaleAnimation.duration = 500
        scaleAnimation.interpolator = BounceInterpolator()
        bt_favorite.setOnCheckedChangeListener { compoundButton, b ->
            compoundButton?.startAnimation(scaleAnimation)
            presenter.updateFavorite(b)
        }
        bt_back.setOnClickListener {
            finish()
        }
    }

    override fun showLoading() {
        DialogManager.showProgress(this)
    }

    override fun hideLoading() {
        DialogManager.hideProgress()
    }

    override fun displayTitle(title: String) {
        tv_title.text = title
    }

    override fun displayPhoto(photoUri: String) {

        val bitmap = LayoutInflater.from(this).toBitmap(R.layout.layout_image_error,
                width = resources.getDimensionPixelSize(R.dimen.movie_list_item_width),
                height = resources.getDimensionPixelSize(R.dimen.movie_list_item_height))
        val errorView = BitmapDrawable(resources, bitmap)

        Glide.with(this)
                .load(photoUri)
                .dontAnimate()
                .error(errorView)
                .into(iv_image)
    }

    override fun displayReleaseDate(releaseDate: String) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun displayRating(stars: Double, reviewCount: String) {
        rb_review.visibility = View.VISIBLE
        rb_review.rating = (stars / 10 * 5).toFloat()

        tv_review_count.visibility = View.VISIBLE
        tv_review_count.text = "$reviewCount reviews"
    }

    override fun displayPlot(plot: String) {
        tv_description.visibility = View.VISIBLE
        tv_description.text = plot
    }

    override fun displaySavedIndicator(isSaved: Boolean) {
        if (loadedFromLocal) return
        bt_favorite.visibility = View.VISIBLE
        bt_favorite.isChecked = isSaved
    }
}
