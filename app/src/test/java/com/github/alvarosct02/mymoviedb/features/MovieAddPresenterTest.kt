package com.github.alvarosct02.mymoviedb.features

import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.local.IDataSourceLocal
import com.github.alvarosct02.mymoviedb.data.source.remote.IDataSourceRemote
import com.github.alvarosct02.mymoviedb.features.movies.add.MovieAddPresenter
import com.github.alvarosct02.mymoviedb.features.movies.add.MovieAddView
import com.github.alvarosct02.mymoviedb.utils.DataSourceRepositoryTest
import com.github.alvarosct02.mymoviedb.utils.RxImmediateSchedulerRule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class MovieAddPresenterTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()
    private val USER_ID = "USER_ID"

    @Mock
    private lateinit var mockLocal: IDataSourceLocal

    @Mock
    private lateinit var mockRemote: IDataSourceRemote

    @Mock
    private lateinit var mockRepo: DataSource

    @Mock
    private lateinit var mockPresenter: MovieAddPresenter

    @Mock
    private lateinit var mockView: MovieAddView

    @Before
    fun setUp() {
        mockRepo = DataSourceRepositoryTest(USER_ID, mockRemote, mockLocal)
        mockPresenter = MovieAddPresenter(mockView, mockRepo)
    }

    @Test
    fun `test search fails when all keywords empty`() {

        mockPresenter.searchMovies(listOf("", "", ""))

        verify(mockView).showEmptyFieldError()
    }

    @Test
    fun `test search fails when at least one keyword empty`() {

        mockPresenter.searchMovies(listOf("", "A", "B"))

        verify(mockView).showEmptyFieldError()
    }

    @Test
    fun `test search fails when keyword with only spaces`() {

        mockPresenter.searchMovies(listOf("   ", "A", "B"))

        verify(mockView).showEmptyFieldError()
    }

    @Test
    fun `test search fails when keywords are duplicated`() {

        mockPresenter.searchMovies(listOf("B", "A", "A"))

        verify(mockView).showDuplicateFieldError()
    }

    @Test
    fun `test search fails when keywords are duplicated with spaces`() {

        mockPresenter.searchMovies(listOf("B", "A ", "A"))

        verify(mockView).showDuplicateFieldError()
    }

    @Test
    fun `test empty fields are evaluated before duplication`() {

        mockPresenter.searchMovies(listOf("", "A ", "A"))

        verify(mockView).showEmptyFieldError()
        verify(mockView, Mockito.never()).showDuplicateFieldError()
    }

    @Test
    fun `test a successful search display next screen`() {

        val keywords = listOf("E", "AB", "A")
        mockPresenter.searchMovies(keywords)
        verify(mockView).onSuccess()
    }


    @Test
    fun `test a successful search is storing the keywords`() {

        val keywords = listOf("E", "AB", "A")
        mockPresenter.searchMovies(keywords)

        verify(mockLocal).saveKeywords(keywords)
        verify(mockView).onSuccess()
    }

}