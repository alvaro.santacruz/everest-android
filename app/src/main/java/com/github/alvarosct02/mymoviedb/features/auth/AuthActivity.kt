package com.github.alvarosct02.mymoviedb.features.auth

import android.os.Bundle
import android.view.View
import androidx.navigation.findNavController
import com.crashlytics.android.Crashlytics
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.features.base.BaseActivity
import com.github.alvarosct02.mymoviedb.utils.hideKeyboard
import kotlinx.android.synthetic.main.layout_toolbar.*

class AuthActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        findNavController(R.id.nav_host_fragment).addOnDestinationChangedListener { controller, destination, arguments ->
            findViewById<View>(android.R.id.content).hideKeyboard()
        }

        runCatching { Crashlytics.setUserIdentifier("Guest") }

    }
}
