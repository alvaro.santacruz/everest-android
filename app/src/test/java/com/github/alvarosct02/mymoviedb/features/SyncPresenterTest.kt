package com.github.alvarosct02.mymoviedb.features

import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.features.sync.SyncPresenter
import com.github.alvarosct02.mymoviedb.features.sync.SyncView
import com.github.alvarosct02.mymoviedb.utils.RxImmediateSchedulerRule
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class SyncPresenterTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @Mock
    private lateinit var mockRepo: DataSource

    @Mock
    private lateinit var mockPresenter: SyncPresenter

    @Mock
    private lateinit var mockView: SyncView

    @Before
    fun setUp() {
        mockPresenter = SyncPresenter(mockView, mockRepo)
    }

    @Test
    fun `test uploadMovies with success`() {

        `when`(mockRepo.deleteMoviesOfCurrentUser(anyList())).thenReturn(Completable.complete())
        `when`(mockRepo.addMoviesOfCurrentUser(anyList())).thenReturn(Completable.complete())
        `when`(mockRepo.getSavedMovies()).thenReturn(Single.just(listOf()))

        mockPresenter.uploadMovies()

        verify(mockView).showLoading()
        verify(mockView).onUploadSuccess()
        verify(mockView).hideLoading()
    }

    @Test
    fun `test uploadMovies with failure`() {

        `when`(mockRepo.deleteMoviesOfCurrentUser(anyList())).thenReturn(Completable.error(Exception("")))
        `when`(mockRepo.getSavedMovies()).thenReturn(Single.just(listOf()))

        mockPresenter.uploadMovies()

        verify(mockView).showLoading()
        verify(mockView).onUploadFails(anyString())
        verify(mockView).hideLoading()
    }

    @Test
    fun `test downloadMovies with success`() {

        `when`(mockRepo.getMyMoviesFromCloud()).thenReturn(Single.just(listOf()))
        `when`(mockRepo.saveMoviesLocally(anyList())).thenReturn(Completable.complete())

        mockPresenter.downloadMovies()

        verify(mockView).showLoading()
        verify(mockView).onDownloadSuccess()
        verify(mockView).hideLoading()
    }

    @Test
    fun `test downloadMovies with failure`() {

        `when`(mockRepo.getMyMoviesFromCloud()).thenReturn(Single.error(Exception("")))

        mockPresenter.downloadMovies()

        verify(mockView).showLoading()
        verify(mockView).onDownloadFails(anyString())
        verify(mockView).hideLoading()
    }

}