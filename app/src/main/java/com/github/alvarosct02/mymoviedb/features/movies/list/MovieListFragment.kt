package com.github.alvarosct02.mymoviedb.features.movies.list

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.features.base.BaseFragment
import com.github.alvarosct02.mymoviedb.features.movies.detail.MovieDetailActivity
import com.github.alvarosct02.mymoviedb.utils.Constants
import com.github.alvarosct02.mymoviedb.utils.SpaceItemDecoration
import com.github.alvarosct02.mymoviedb.utils.adt.BinarySearchTree
import com.github.alvarosct02.mymoviedb.utils.adt.DoublyLinkedList
import kotlinx.android.synthetic.main.fragment_movie_list.*

class MovieListFragment : BaseFragment(), MovieListView {

    private lateinit var presenter: MovieListPresenter
    private lateinit var moviesAdapter: MovieCategoryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movie_list, container, false)
    }

    override fun setupVariables() {
        super.setupVariables()
        val shouldLoadFromLocal = arguments?.getBoolean(getString(R.string.extra_load_from_local))
                ?: Constants.LOAD_FROM_LOCAL_DEFAULT
        presenter = if (shouldLoadFromLocal) MovieListFromLocalPresenter(this) else MovieListPresenter(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.searchMovies()
    }

    override fun setupViews() {
        super.setupViews()

        rv_movie_categories.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            addItemDecoration(SpaceItemDecoration(context, inBetween = 16))
        }

        bt_add_movie.setOnClickListener {
            findNavController().navigate(R.id.nav_add_more_movies)
        }

    }

    override fun displayMovies(movies: DoublyLinkedList<Pair<String, BinarySearchTree<Movie>>>, loadedFromLocal: Boolean) {
        group_no_movies.visibility = View.GONE
        rv_movie_categories.visibility = View.VISIBLE
        moviesAdapter = MovieCategoryAdapter(movies) { movie, transitionViews ->
            openDetail(movie, transitionViews[0], loadedFromLocal)
        }
        rv_movie_categories.adapter = moviesAdapter
    }

    private fun openDetail(movie: Movie, sharedElement: View, loadedFromLocal: Boolean) {
        val intent = Intent(requireContext(), MovieDetailActivity::class.java).apply {
            putExtra(Constants.EXTRA_MOVIE_ID, movie.imdbID)
            putExtra(Constants.EXTRA_MOVIE_KEYWORD, movie.keywordUsed)
            putExtra(Constants.EXTRA_MOVIE_IMAGE_TRANSITION_ID, ViewCompat.getTransitionName(sharedElement))
            putExtra(Constants.EXTRA_LOAD_FROM_LOCAL, loadedFromLocal)
        }
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity(),
                sharedElement,
                ViewCompat.getTransitionName(sharedElement)?: "")
        startActivity(intent, options.toBundle())

    }

    override fun displayNoMoviesMessage() {
        group_no_movies.visibility = View.VISIBLE
        rv_movie_categories.visibility = View.GONE
    }
}
