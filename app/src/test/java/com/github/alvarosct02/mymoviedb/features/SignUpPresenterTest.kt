package com.github.alvarosct02.mymoviedb.features

import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.local.IDataSourceLocal
import com.github.alvarosct02.mymoviedb.data.source.remote.IDataSourceRemote
import com.github.alvarosct02.mymoviedb.features.sign_up.SignUpPresenter
import com.github.alvarosct02.mymoviedb.features.sign_up.SignUpView
import com.github.alvarosct02.mymoviedb.utils.DataSourceRepositoryTest
import com.github.alvarosct02.mymoviedb.utils.RxImmediateSchedulerRule
import io.reactivex.Completable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class SignUpPresenterTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()
    private val USER_ID = "USER_ID"
    private val NAME = "Alvaro"
    private val EMAIL = "alvaro.santacruz@demo.com"
    private val EMAIL_BAD = "alvaro.santacruz"
    private val PASSWORD = "HelloWorld1234$"
    private val PASSWORD_SHORT = "Hello"
    private val ERROR = "Error"

    @Mock
    private lateinit var mockLocal: IDataSourceLocal

    @Mock
    private lateinit var mockRemote: IDataSourceRemote

    @Mock
    private lateinit var mockRepo: DataSource

    @Mock
    private lateinit var mockPresenter: SignUpPresenter

    @Mock
    private lateinit var mockView: SignUpView

    @Before
    fun setUp() {
        mockRepo = DataSourceRepositoryTest(USER_ID, mockRemote, mockLocal)
        mockPresenter = SignUpPresenter(mockView, mockRepo)
    }

    @Test
    fun `test all fields empty`() {

        mockPresenter.signUpUser("", "", "", "")

        verify(mockView).showValidationError(R.string.message_error_empty_fields)
    }

    @Test
    fun `test some fields empty`() {

        mockPresenter.signUpUser(NAME, EMAIL, "", "")

        verify(mockView).showValidationError(R.string.message_error_empty_fields)
    }

    @Test
    fun `test only one message is being showed`() {

        mockPresenter.signUpUser(NAME, EMAIL, "", "")

        verify(mockView).showValidationError(R.string.message_error_empty_fields)
        verify(mockView, never()).showValidationError(R.string.message_error_password_length)
    }

    @Test
    fun `test email should follow email pattern`() {

        mockPresenter.signUpUser(NAME, EMAIL_BAD, PASSWORD, PASSWORD)

        verify(mockView).showValidationError(R.string.message_error_email_format)
    }

    @Test
    fun `test password lenght validation`() {

        mockPresenter.signUpUser(NAME, EMAIL, PASSWORD_SHORT, PASSWORD)

        verify(mockView).showValidationError(R.string.message_error_password_length)
    }

    @Test
    fun `test password and password confirmation must match`() {

        mockPresenter.signUpUser(NAME, EMAIL, PASSWORD, PASSWORD.plus("A"))

        verify(mockView).showValidationError(R.string.message_error_password_confirmation)
    }

    @Test
    fun `test email error has precendence over password validation`() {

        mockPresenter.signUpUser(NAME, EMAIL_BAD, PASSWORD_SHORT, PASSWORD)

        verify(mockView).showValidationError(R.string.message_error_email_format)
        verify(mockView, never()).showValidationError(R.string.message_error_password_length)
    }

    @Test
    fun `test sign up success`() {

        `when`(mockRemote.signUpUserWithEmail(NAME, EMAIL, PASSWORD)).thenReturn(Completable.complete())

        mockPresenter.signUpUser(NAME, EMAIL, PASSWORD, PASSWORD)

        verify(mockView).onSignUpSuccess()
    }

    @Test
    fun `test sign up error`() {

        `when`(mockRemote.signUpUserWithEmail(NAME, EMAIL, PASSWORD)).thenReturn(Completable.error(Throwable(ERROR)))

        mockPresenter.signUpUser(NAME, EMAIL, PASSWORD, PASSWORD)

        verify(mockView).onSignUpFails(ERROR)
    }

    @Test
    fun `test dialog is displaying correctly on sign up success`() {

        `when`(mockRemote.signUpUserWithEmail(NAME, EMAIL, PASSWORD)).thenReturn(Completable.complete())

        mockPresenter.signUpUser(NAME, EMAIL, PASSWORD, PASSWORD)

        verify(mockView).showLoading()
        verify(mockView).hideLoading()
    }

    @Test
    fun `test dialog is displaying correctly on sign up error`() {

        `when`(mockRemote.signUpUserWithEmail(NAME, EMAIL, PASSWORD)).thenReturn(Completable.error(Throwable(ERROR)))

        mockPresenter.signUpUser(NAME, EMAIL, PASSWORD, PASSWORD)

        verify(mockView).showLoading()
        verify(mockView).hideLoading()
    }
}