package com.github.alvarosct02.mymoviedb.features.movies.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.utils.SpaceItemDecoration
import com.github.alvarosct02.mymoviedb.utils.adt.BinarySearchTree
import com.github.alvarosct02.mymoviedb.utils.adt.DoublyLinkedList
import kotlinx.android.synthetic.main.item_movie_category.view.*

class MovieCategoryAdapter(
        private val movies: DoublyLinkedList<Pair<String, BinarySearchTree<Movie>>>,
        private val callback: (Movie, List<View>) -> Unit
) : RecyclerView.Adapter<MovieCategoryAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        val obj = movies.getItemAt(pos)!!

        holder.tvTitle.text = "${obj.first} (${obj.second.size()})"
        holder.rvMovie.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            addItemDecoration(SpaceItemDecoration(context, inBetween = 8, left = 16, right = 16))
            adapter = MovieAdapter(obj.second, callback)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_movie_category, parent, false))
    }

    override fun getItemCount() = movies.size()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvTitle = view.tv_title
        val rvMovie = view.rv_movie
    }
}