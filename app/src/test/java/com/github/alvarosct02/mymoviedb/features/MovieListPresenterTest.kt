package com.github.alvarosct02.mymoviedb.features

import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.data.source.DataSourceRepository
import com.github.alvarosct02.mymoviedb.features.movies.list.MovieListPresenter
import com.github.alvarosct02.mymoviedb.features.movies.list.MovieListView
import com.github.alvarosct02.mymoviedb.utils.RxImmediateSchedulerRule
import com.github.alvarosct02.mymoviedb.utils.adt.BinarySearchTree
import com.github.alvarosct02.mymoviedb.utils.adt.DoublyLinkedList
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.*
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class MovieListPresenterTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    private val KEYWORD_A = "KEYWORD_A"
    private val KEYWORD_B = "KEYWORD_B"

    @Mock
    private lateinit var mockRepo: DataSourceRepository

    @Mock
    private lateinit var mockPresenter: MovieListPresenter

    @Mock
    private lateinit var mockView: MovieListView

    private val movieList: DoublyLinkedList<Pair<String, BinarySearchTree<Movie>>> = DoublyLinkedList()

    private lateinit var movie1: Movie
    private lateinit var movie2: Movie
    private lateinit var movie3: Movie
    private lateinit var movie4: Movie

    @Before
    fun setUp() {
        mockPresenter = MovieListPresenter(mockView, mockRepo, movieList)
        movie1 = mockMovie(KEYWORD_A)
        movie2 = mockMovie(KEYWORD_A)
        movie3 = mockMovie(KEYWORD_A)
        movie4 = mockMovie(KEYWORD_B)
    }

    private fun mockMovie(keyword: String) = Movie(
            title = "",
            year = "",
            imdbID = "",
            type = "",
            poster = "",
            keywordUsed = keyword,
            userId = ""
    )

    @Test
    fun `test searchMovie being called for each keyword`() {

        val keywords = DoublyLinkedList<String>().apply {
            addFirst(KEYWORD_A)
            addFirst(KEYWORD_B)
        }
        `when`(mockRepo.getKeywords()).thenReturn(keywords)
        `when`(mockRepo.searchMovies(anyList())).thenCallRealMethod()
        `when`(mockRepo.searchMovie(anyString())).thenReturn(Single.just(listOf()))

        mockPresenter.searchMovies()

        verify(mockRepo, times(keywords.size())).searchMovie(anyString())
    }

    @Test
    fun `test movies being displayed`() {

        val keywords = DoublyLinkedList<String>().apply {
            addFirst(KEYWORD_A)
            addFirst(KEYWORD_B)
        }
        `when`(mockRepo.getKeywords()).thenReturn(keywords)
        `when`(mockRepo.searchMovies(anyList())).thenCallRealMethod()
        `when`(mockRepo.searchMovie(anyString()))
                .thenReturn(Single.just(listOf(movie1)))

        mockPresenter.searchMovies()

        verify(mockView).displayMovies(movieList,false)
        verify(mockView, never()).displayNoMoviesMessage()

    }

    @Test
    fun `test no movies being displayed`() {

        val keywords = DoublyLinkedList<String>().apply {
            addFirst(KEYWORD_A)
            addFirst(KEYWORD_B)
        }
        `when`(mockRepo.getKeywords()).thenReturn(keywords)
        `when`(mockRepo.searchMovies(anyList())).thenCallRealMethod()
        `when`(mockRepo.searchMovie(anyString()))
                .thenReturn(Single.just(listOf()))

        mockPresenter.searchMovies()

        verify(mockView).displayNoMoviesMessage()

    }
}