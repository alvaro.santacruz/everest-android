package com.github.alvarosct02.mymoviedb.data.source

import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.data.source.local.DataSourceLocal
import com.github.alvarosct02.mymoviedb.data.source.local.IDataSourceLocal
import com.github.alvarosct02.mymoviedb.data.source.remote.DataSourceRemote
import com.github.alvarosct02.mymoviedb.data.source.remote.IDataSourceRemote
import com.github.alvarosct02.mymoviedb.utils.adt.DoublyLinkedList
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

open class DataSourceRepository(
        private val remote: IDataSourceRemote = DataSourceRemote(),
        private val local: IDataSourceLocal = DataSourceLocal()
) : DataSource {

    override fun searchMovie(term: String): Single<List<Movie>> {
        return remote.searchMovie(term).onErrorReturn { listOf() }
    }

    override fun searchMovies(terms: List<String>): Single<List<Movie>> {
        return Observable.fromIterable(terms)
                .flatMap { term -> searchMovie(term).toObservable().flatMapIterable { it } }
                .toList()
    }

    override fun getSavedMovies(): Single<List<Movie>> {
        return local.getSavedMovies()
    }

    override fun resetKeywords() {
        local.resetKeywords()
    }

    override fun saveKeywords(keywords: List<String>) {
        local.saveKeywords(keywords)
    }

    override fun saveNewKeyword(keyword: String) {
        local.saveKeywords(keyword)
    }

    override fun getKeywords(): DoublyLinkedList<String> {
        return local.getKeywords()
    }

    override fun saveMoviesLocally(movies: List<Movie>): Completable {
        return local.saveMovies(movies)
    }

    override fun deleteMovieLocally(movie: Movie): Completable {
        return local.forgetMovie(movie)
    }

    override fun saveMovie(movie: Movie): Completable {
        movie.apply {
            userId = getUserId()
            isSaved = true
        }
        return local.saveMovie(movie)
    }

    override fun forgetMovie(movie: Movie): Completable {
        movie.isSaved = false
        return local.updateMovie(movie)
    }

    override fun getMovie(id: String): Single<Movie> {
        return local.getSavedMovie(id).onErrorResumeNext {
            remote.getMovie(id)
        }
    }

    override fun deleteMoviesOfCurrentUser(movies: List<Movie>): Completable {
        return remote.deleteMoviesOfUser(getUserId(), movies)
    }

    override fun addMoviesOfCurrentUser(movies: List<Movie>): Completable {
        return remote.addMoviesOfUser(getUserId(), movies)
    }

    override fun getMyMoviesFromCloud(): Single<List<Movie>> {
        return remote.getMyMoviesFromCloud(getUserId())
    }

    protected open fun getUserId(): String {
        return FirebaseAuth.getInstance().currentUser!!.uid
    }

    override fun logout(): Completable {
        return local.logout()
    }

    override fun loginUserWithEmail(email: String, password: String): Completable {
        return remote.loginUserWithEmail(email, password)
    }

    override fun signUpUserWithEmail(name: String, email: String, password: String): Completable {
        return remote.signUpUserWithEmail(name, email, password)
    }

    override fun sendPasswordResetEmail(email: String): Completable {
        return remote.sendPasswordResetEmail(email)
    }
}