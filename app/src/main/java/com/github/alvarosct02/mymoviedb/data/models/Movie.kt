package com.github.alvarosct02.mymoviedb.data.models

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["keywordUsed", "imdbID"])
open class Movie(
        @SerializedName("Title") val title: String,
        @SerializedName("Year") val year: String,
        @SerializedName("imdbID") val imdbID: String,
        @SerializedName("Type") val type: String,
        @SerializedName("Poster") val poster: String,

        @SerializedName("keyword_used") var keywordUsed: String,
        @SerializedName("user_id") var userId: String,

        var isSaved: Boolean = false,

        @SerializedName("Rated") val rated: String? = null,
        @SerializedName("Released") val released: String? = null,
        @SerializedName("Runtime") val runtime: String? = null,
        @SerializedName("Genre") val genre: String? = null,
        @SerializedName("Director") val director: String? = null,
        @SerializedName("Writer") val writer: String? = null,
        @SerializedName("Actors") val actors: String? = null,
        @SerializedName("Plot") val plot: String? = null,
        @SerializedName("Language") val language: String? = null,
        @SerializedName("Country") val country: String? = null,
        @SerializedName("Awards") val awards: String? = null,
//        @SerializedName("Ratings") val ratings: List<Ratings>? = null,
        @SerializedName("Metascore") val metascore: String? = null,
        @SerializedName("imdbRating") val imdbRating: String? = null,
        @SerializedName("imdbVotes") val imdbVotes: String? = null,
        @SerializedName("DVD") val dVD: String? = null,
        @SerializedName("BoxOffice") val boxOffice: String? = null,
        @SerializedName("Production") val production: String? = null,
        @SerializedName("Website") val website: String? = null,
        @SerializedName("Response") val response: Boolean? = null

) {
    fun uniqueId() = keywordUsed.plus(imdbID)

    constructor() : this("", "", "", "", "", "", "", false)

}

data class Ratings(
        @SerializedName("source") val source: String,
        @SerializedName("value") val value: String
)