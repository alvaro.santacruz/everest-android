package com.github.alvarosct02.mymoviedb.features.main

interface MainView {

    fun goToHome()

    fun displayErrorMessage(message: String)

    fun navigateToMenuItem(id: Int)

}
