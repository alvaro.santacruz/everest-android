package com.github.alvarosct02.mymoviedb.features.movies.add

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.utils.afterTextChanged
import kotlinx.android.synthetic.main.item_movie_keyword.view.*

class MovieKeywordAdapter(
        private val items: MutableList<String>
) : RecyclerView.Adapter<MovieKeywordAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        val obj = items[pos]

        holder.etKeyword.setText(obj)
        holder.tilKeyword.hint = holder.tilKeyword.context.getString(R.string.add_movie_placeholder_movie, pos + 1)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_movie_keyword, parent, false)).apply {
            itemView.et_keyword.afterTextChanged {
                items[adapterPosition] = it
            }

            itemView.et_keyword.setOnFocusChangeListener { v, isFocused ->
                val colorRes = if (isFocused) R.color.colorTextFieldActivate else R.color.colorTextField
                itemView.til_keyword.boxBackgroundColor = ContextCompat.getColor(v.context, colorRes)
            }
        }
    }

    override fun getItemCount() = items.size

    fun getKeywords() = items

    fun clear() {
        for (i in 0 until items.size) {
            items[i] = ""
        }
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val etKeyword = view.et_keyword
        val tilKeyword = view.til_keyword
    }
}