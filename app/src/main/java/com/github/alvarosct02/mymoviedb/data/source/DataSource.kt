package com.github.alvarosct02.mymoviedb.data.source

import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.utils.adt.DoublyLinkedList
import io.reactivex.Completable
import io.reactivex.Single

interface DataSource {

    fun searchMovie(term: String): Single<List<Movie>>

    fun searchMovies(terms: List<String>): Single<List<Movie>>

    fun getSavedMovies(): Single<List<Movie>>

    fun resetKeywords()

    fun saveKeywords(keywords: List<String>)

    fun saveNewKeyword(keyword: String)

    fun getKeywords(): DoublyLinkedList<String>

    fun saveMoviesLocally(movies: List<Movie>): Completable

    fun deleteMovieLocally(movie: Movie): Completable

    fun saveMovie(movie: Movie): Completable

    fun forgetMovie(movie: Movie): Completable

    fun getMovie(id: String): Single<Movie>

    fun deleteMoviesOfCurrentUser(movies: List<Movie>): Completable

    fun addMoviesOfCurrentUser(movies: List<Movie>): Completable

    fun getMyMoviesFromCloud(): Single<List<Movie>>

    fun logout(): Completable

    fun loginUserWithEmail(email: String, password: String): Completable

    fun signUpUserWithEmail(name: String, email: String, password: String): Completable

    fun sendPasswordResetEmail(email: String): Completable


}