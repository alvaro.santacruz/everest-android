package com.github.alvarosct02.mymoviedb.features.login

import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.DataSourceRepository
import com.github.alvarosct02.mymoviedb.features.base.BasePresenter
import com.github.alvarosct02.mymoviedb.utils.isValidEmail
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class LoginPresenter(
        view: LoginView,
        private val dataRepo: DataSource = DataSourceRepository()
) : BasePresenter<LoginView>(view) {

    fun loginUser(email: String, password: String) {

        if (!areAllFieldsValid(email, password)) return

        view.showLoading()

        disposable.add(dataRepo.loginUserWithEmail(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideLoading()
                    view.onLoginSuccess()
                }, {
                    view.hideLoading()
                    view.onLoginFails(it.message)
                })
        )
    }

    private fun areAllFieldsValid(email: String, password: String): Boolean {
        when {
            email.isBlank() -> view.showValidationError(R.string.message_error_empty_fields)
            password.isBlank() -> view.showValidationError(R.string.message_error_empty_fields)
            !isValidEmail(email) -> view.showValidationError(R.string.message_error_email_format)
            password.length < 8 -> view.showValidationError(R.string.message_error_password_length)
            else -> return true
        }
        return false
    }

}
