package com.github.alvarosct02.mymoviedb.data.source.local

import android.util.Log
import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.utils.adt.DoublyLinkedList
import io.reactivex.Completable
import io.reactivex.Single

open class DataSourceLocal(
        private val database: AppDatabase = AppDatabase.getInstance()
) : IDataSourceLocal {

    companion object {
        const val TAG = "DataSourceLocal"
    }

    override fun saveMovies(movie: List<Movie>): Completable {
        return Completable.fromAction {
            val result = database.movieDao().insertAllMovies(*movie.toTypedArray())
            result.forEach {
                Log.e("ASCT", it.toString())
            }
        }
    }

    override fun saveMovie(movie: Movie): Completable {
        return Completable.fromAction {
            Log.d(TAG, "Saving: $movie")
            database.movieDao().insertMovie(movie)
        }
    }

    override fun updateMovie(movie: Movie): Completable {
        return Completable.fromAction {
            Log.d(TAG, "Updating: $movie")
            database.movieDao().insertMovie(movie)
        }
    }

    override fun forgetMovie(movie: Movie): Completable {
        return Completable.fromAction {
            Log.d(TAG, "Deleting: $movie")
            database.movieDao().deleteById(movie.imdbID, movie.keywordUsed)
        }
    }

    override fun resetKeywords() {
        SessionManager.resetKeywordList()
    }

    override fun saveKeywords(keywords: List<String>) {
        SessionManager.addToKeywordList(keywords)
    }

    override fun saveKeywords(keyword: String) {
        SessionManager.addToKeywordList(listOf(keyword))
    }

    override fun getKeywords(): DoublyLinkedList<String> {
        return DoublyLinkedList<String>().apply {
            SessionManager.getKeywordList().forEach {
                addFirst(it)
            }
        }
    }

    override fun getSavedMovies(): Single<List<Movie>> {
        Log.d(TAG, "Getting saved movies")
        return database.movieDao().getAllMovies()
    }

    override fun getSavedMovie(id: String): Single<Movie> {
        Log.d(TAG, "Getting saved movie by id: $id")
        return database.movieDao().getMovieById(id)
    }

    override fun logout(): Completable {
        return Completable.fromAction {
            resetKeywords()
            database.movieDao().deleteAll()
        }
    }

}