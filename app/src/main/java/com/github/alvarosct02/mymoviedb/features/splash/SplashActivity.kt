package com.github.alvarosct02.mymoviedb.features.splash

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewAnimationUtils
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.features.auth.AuthActivity
import com.github.alvarosct02.mymoviedb.features.base.BaseActivity
import com.github.alvarosct02.mymoviedb.features.main.MainActivity
import kotlinx.android.synthetic.main.activity_splash.*
import kotlin.concurrent.thread
import kotlin.math.hypot


class SplashActivity : BaseActivity(), SplashView {

    private lateinit var presenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setupVariables()

        content_reveal.post {
            startCircularReveal(content_reveal, 800)
            thread {
                Thread.sleep(1500)
                presenter.startApp()
            }
        }

    }

    private fun setupVariables() {
        presenter = SplashPresenter(this)
    }

    private fun startCircularReveal(viewToReveal: View, duration: Int) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return

        val cx = viewToReveal.width / 2.0
        val cy = viewToReveal.height / 2.0

        val finalRadius = hypot(cx, cy).toFloat() * 1.1f
        val reveal = ViewAnimationUtils.createCircularReveal(
                viewToReveal, cx.toInt(), cy.toInt(), 0f, finalRadius)
        reveal.duration = duration.toLong()
        reveal.start()
    }

    override fun goToHome() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun goToApp() {
        startActivity(Intent(this, AuthActivity::class.java))
        finish()
    }
}
