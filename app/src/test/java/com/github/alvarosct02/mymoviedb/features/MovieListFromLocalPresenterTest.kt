package com.github.alvarosct02.mymoviedb.features

import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.DataSourceRepository
import com.github.alvarosct02.mymoviedb.features.movies.list.MovieListFromLocalPresenter
import com.github.alvarosct02.mymoviedb.features.movies.list.MovieListPresenter
import com.github.alvarosct02.mymoviedb.features.movies.list.MovieListView
import com.github.alvarosct02.mymoviedb.utils.RxImmediateSchedulerRule
import com.github.alvarosct02.mymoviedb.utils.adt.BinarySearchTree
import com.github.alvarosct02.mymoviedb.utils.adt.DoublyLinkedList
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class MovieListFromLocalPresenterTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    private val KEYWORD_A = "KEYWORD_A"
    private val KEYWORD_B = "KEYWORD_B"


    @Mock
    private lateinit var mockRepo: DataSourceRepository

    @Mock
    private lateinit var mockPresenter: MovieListPresenter

    @Mock
    private lateinit var mockView: MovieListView

    private val movieList: DoublyLinkedList<Pair<String, BinarySearchTree<Movie>>> = DoublyLinkedList()


    private lateinit var movie1: Movie
    private lateinit var movie2: Movie
    private lateinit var movie3: Movie
    private lateinit var movie4: Movie

    @Before
    fun setUp() {
        mockPresenter = MovieListFromLocalPresenter(mockView, mockRepo, movieList)
        movie1 = mockMovie(KEYWORD_A)
        movie2 = mockMovie(KEYWORD_A)
        movie3 = mockMovie(KEYWORD_A)
        movie4 = mockMovie(KEYWORD_B)
    }

    private fun mockMovie(keyword: String) = Movie(
        title = "",
        year = "",
        imdbID = "",
        type = "",
        poster = "",
        keywordUsed = keyword,
        userId = ""
    )

    @Test
    fun `test searchMovie is not requesting movies to omdb`() {

        Mockito.`when`(mockRepo.getSavedMovies()).thenReturn(Single.just(listOf()))

        mockPresenter.searchMovies()

        Mockito.verify(mockRepo, Mockito.never()).getKeywords()
        Mockito.verify(mockRepo).getSavedMovies()
    }

    @Test
    fun `test movies being displayed`() {
        Mockito.`when`(mockRepo.getSavedMovies()).thenReturn(Single.just(listOf(movie1, movie2)))

        mockPresenter.searchMovies()

        Mockito.verify(mockView).displayMovies(movieList,true)
        Mockito.verify(mockView, Mockito.never()).displayNoMoviesMessage()

    }

    @Test
    fun `test no movies being displayed`() {
        Mockito.`when`(mockRepo.getSavedMovies()).thenReturn(Single.just(listOf()))

        mockPresenter.searchMovies()

        Mockito.verify(mockView).displayNoMoviesMessage()

    }
}