package com.github.alvarosct02.mymoviedb.features.base

interface LoadingView {

    fun showLoading()

    fun hideLoading()

}
