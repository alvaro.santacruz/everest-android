package com.github.alvarosct02.mymoviedb.features.movies.add

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.features.base.BaseFragment
import com.github.alvarosct02.mymoviedb.utils.Constants
import com.github.alvarosct02.mymoviedb.utils.DialogManager
import com.github.alvarosct02.mymoviedb.utils.SpaceItemDecoration
import com.github.alvarosct02.mymoviedb.utils.hideKeyboard
import kotlinx.android.synthetic.main.fragment_movie_add.*

class MovieAddFragment : BaseFragment(), MovieAddView {

    private var numberOfMoviesToAdd: Int = 0
    private lateinit var presenter: MovieAddPresenter
    private lateinit var keywordAdapter: MovieKeywordAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movie_add, container, false)
    }

    override fun setupVariables() {
        super.setupVariables()
        numberOfMoviesToAdd = arguments?.getInt(getString(R.string.extra_movie_add_max_number))
                ?: Constants.MOVIE_ADD_MAX_NUMBER_DEFAULT
        presenter = MovieAddPresenter(this)
    }

    override fun setupViews() {
        super.setupViews()

        val keywords = MutableList(numberOfMoviesToAdd) { "" }

        keywordAdapter = MovieKeywordAdapter(keywords)
        rv_movie_fields.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            addItemDecoration(SpaceItemDecoration(context, inBetween = 16))
            adapter = keywordAdapter
        }

        bt_search.setOnClickListener {
            it.hideKeyboard()
            presenter.searchMovies(keywordAdapter.getKeywords())
        }

        bt_clear.setOnClickListener {
            it.hideKeyboard()
            keywordAdapter.clear()
        }
    }

    override fun onSuccess() {
        findNavController().navigate(R.id.goTo_list_movies)
    }

    override fun showEmptyFieldError() {
        val message = getString(R.string.add_movie_error_empty)
        DialogManager.showMessage(requireContext(), message)
    }

    override fun showDuplicateFieldError() {
        val message = getString(R.string.add_movie_error_duplicate)
        DialogManager.showMessage(requireContext(), message)
    }
}
