package com.github.alvarosct02.mymoviedb.features.splash

import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.DataSourceRepository
import com.github.alvarosct02.mymoviedb.features.base.BasePresenter
import com.google.firebase.auth.FirebaseAuth

open class SplashPresenter(
        view: SplashView,
        private val auth: FirebaseAuth = FirebaseAuth.getInstance(),
        private val dataSourceRepository: DataSource = DataSourceRepository()
) : BasePresenter<SplashView>(view) {

    fun startApp() {
        if (auth.currentUser != null) {
            dataSourceRepository.resetKeywords()
            view.goToHome()
        } else {
            view.goToApp()
        }
    }

}
