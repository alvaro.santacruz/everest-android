package com.github.alvarosct02.mymoviedb.data.pojos

import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.google.gson.annotations.SerializedName

data class SearchMovieResponse(
    @SerializedName("Search")
    val movies: List<Movie>
)

