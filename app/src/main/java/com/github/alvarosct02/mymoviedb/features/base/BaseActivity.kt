package com.github.alvarosct02.mymoviedb.features.base

import androidx.appcompat.app.AppCompatActivity
import com.github.alvarosct02.mymoviedb.utils.DialogManager

abstract class BaseActivity: AppCompatActivity(), LoadingView{

    override fun showLoading() {
        DialogManager.showProgress(this)
    }

    override fun hideLoading() {
        DialogManager.hideProgress()
    }
}
