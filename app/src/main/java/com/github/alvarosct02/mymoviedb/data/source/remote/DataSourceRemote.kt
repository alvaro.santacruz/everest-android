package com.github.alvarosct02.mymoviedb.data.source.remote

import com.github.alvarosct02.mymoviedb.BuildConfig
import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import io.reactivex.Completable
import io.reactivex.Single

open class DataSourceRemote : IDataSourceRemote {

    override fun searchMovie(term: String): Single<List<Movie>> {
        return RequestManager().getApi().search(BuildConfig.OMDB_API_KEY, term)
                .map { it.movies }
                .flatMapIterable { it }
                .distinct(Movie::imdbID)
                .map {
                    it.apply {
                        keywordUsed = term
                    }
                }
                .toList()
    }

    override fun getMovie(id: String): Single<Movie> {
        return RequestManager().getApi().getMovie(BuildConfig.OMDB_API_KEY, id)
                .singleOrError()
    }

    override fun getMyMoviesFromCloud(userId: String): Single<List<Movie>> {
        return Single.create { emitter ->
            val listenerRegistration = FirestoreManager.getMoviesByUser(userId)
                    .addSnapshotListener { snapshot, e ->
                        when {
                            e != null -> emitter.onError(e)
                            snapshot != null -> {
                                try {
                                    val movieList = snapshot.documents.map {
                                        it.toObject(Movie::class.java)!!
                                    }
                                    emitter.onSuccess(movieList)
                                } catch (exception: Exception) {
                                    emitter.onError(exception)
                                }
                            }
                            else -> emitter.onError(Exception("DataSourceRemote: Unhandled Exception"))
                        }
                    }
            emitter.setCancellable { listenerRegistration.remove() }
        }
    }

    override fun deleteMoviesOfUser(userId: String, movieList: List<Movie>): Completable {
        return Completable.fromCallable {
            FirestoreManager.deleteMoviesOfUser(userId, movieList)
        }
    }

    override fun addMoviesOfUser(userId: String, movieList: List<Movie>): Completable {
        return Completable.fromCallable {
            FirestoreManager.addMoviesOfUser(userId, movieList)
        }
    }

    override fun loginUserWithEmail(email: String, password: String): Completable {
        return Completable.create { emitter ->
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnCompleteListener {
                if (it.isSuccessful) {
                    emitter.onComplete()
                } else {
                    emitter.onError(it.exception!!)
                }
            }
        }
    }

    override fun signUpUserWithEmail(name: String, email: String, password: String): Completable {
        return Completable.create { emitter ->
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                    .continueWithTask {
                        val profileUpdates = UserProfileChangeRequest.Builder().setDisplayName(name).build()
                        it.result?.user?.updateProfile(profileUpdates)
                    }
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            emitter.onComplete()
                        } else {
                            emitter.onError(it.exception!!)
                        }
                    }
        }
    }

    override fun sendPasswordResetEmail(email: String): Completable {
        return Completable.create { emitter ->
            FirebaseAuth.getInstance().sendPasswordResetEmail(email).addOnCompleteListener {
                if (it.isSuccessful) {
                    emitter.onComplete()
                } else {
                    emitter.onError(it.exception!!)
                }
            }
        }
    }
}