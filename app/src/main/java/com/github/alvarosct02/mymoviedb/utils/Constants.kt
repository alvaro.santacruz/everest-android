package com.github.alvarosct02.mymoviedb.utils

object Constants {

    const val MOVIE_ADD_MAX_NUMBER_DEFAULT = 5
    const val LOAD_FROM_LOCAL_DEFAULT = false

    const val EXTRA_MOVIE_ID = "EXTRA_MOVIE_ID"
    const val EXTRA_MOVIE_IMAGE_TRANSITION_ID = "EXTRA_MOVIE_IMAGE_TRANSITION_ID"
    const val EXTRA_MOVIE_KEYWORD = "EXTRA_MOVIE_KEYWORD"
    const val EXTRA_LOAD_FROM_LOCAL = "EXTRA_LOAD_FROM_LOCAL"

    const val PREFS_KEYWORDS = "PREFS_KEYWORDS"
}