package com.github.alvarosct02.mymoviedb.utils

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Canvas
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.LayoutRes
import androidx.core.util.PatternsCompat


fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

fun String.convertToInt() = this.replace(",", "").toInt()

fun View.hideKeyboard() {
    val imm = this.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
}

fun LayoutInflater.toBitmap(@LayoutRes layoutId: Int, width: Int, height: Int): Bitmap {
    val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val c = Canvas(bitmap)
    val v = this.inflate(layoutId, null, false)
    v.measure(width, height)
    v.layout(v.left, v.top, v.right, v.bottom)
    v.draw(c)
    return bitmap
}

fun isValidEmail(target: CharSequence): Boolean {
    return !target.isBlank() && PatternsCompat.EMAIL_ADDRESS.matcher(target).matches()
}