package com.github.alvarosct02.mymoviedb.features.movies.list

import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.features.base.LoadingView
import com.github.alvarosct02.mymoviedb.utils.adt.BinarySearchTree
import com.github.alvarosct02.mymoviedb.utils.adt.DoublyLinkedList

interface MovieListView: LoadingView {

    fun displayMovies(movies: DoublyLinkedList<Pair<String, BinarySearchTree<Movie>>>, loadedFromLocal: Boolean)

    fun displayNoMoviesMessage()
}