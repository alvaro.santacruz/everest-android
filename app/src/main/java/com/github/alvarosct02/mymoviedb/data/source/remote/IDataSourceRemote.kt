package com.github.alvarosct02.mymoviedb.data.source.remote

import com.github.alvarosct02.mymoviedb.data.models.Movie
import io.reactivex.Completable
import io.reactivex.Single

interface IDataSourceRemote {

    fun searchMovie(term: String): Single<List<Movie>>

    fun getMovie(id: String): Single<Movie>

    fun getMyMoviesFromCloud(userId: String): Single<List<Movie>>

    fun deleteMoviesOfUser(userId: String, movieList: List<Movie>): Completable

    fun addMoviesOfUser(userId: String, movieList: List<Movie>): Completable

    fun loginUserWithEmail(email: String, password: String): Completable

    fun signUpUserWithEmail(name: String, email: String, password: String): Completable

    fun sendPasswordResetEmail(email: String): Completable
}