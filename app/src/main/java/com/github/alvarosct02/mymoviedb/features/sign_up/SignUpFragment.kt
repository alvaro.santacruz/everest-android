package com.github.alvarosct02.mymoviedb.features.sign_up

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.alvarosct02.mymoviedb.R
import com.github.alvarosct02.mymoviedb.features.base.BaseFragment
import com.github.alvarosct02.mymoviedb.features.main.MainActivity
import com.github.alvarosct02.mymoviedb.utils.DialogManager
import kotlinx.android.synthetic.main.fragment_sign_up.*

class SignUpFragment : BaseFragment(), SignUpView {

    private lateinit var presenter: SignUpPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun setupVariables() {
        super.setupVariables()
        presenter = SignUpPresenter(this)
    }

    override fun setupViews() {
        super.setupViews()

        bt_sign_up.setOnClickListener {
            presenter.signUpUser(
                    name = til_name.editText?.text.toString(),
                    email = til_email.editText?.text.toString(),
                    password = til_password.editText?.text.toString(),
                    passwordConfirmation = til_password_confirm.editText?.text.toString())
        }

    }

    override fun onSignUpFails(message: String?) {
        DialogManager.showMessage(requireContext(), message
                ?: getString(R.string.message_error_default))
    }

    override fun onSignUpSuccess() {
        startActivity(Intent(requireContext(), MainActivity::class.java))
        activity?.finish()
        hideLoading()
    }

    override fun showValidationError(message: String) {
        DialogManager.showMessage(requireContext(), message)
    }

    override fun showValidationError(message: Int) {
        DialogManager.showMessage(requireContext(), message)
    }
}
