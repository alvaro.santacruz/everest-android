package com.github.alvarosct02.mymoviedb

import android.util.Log
import androidx.multidex.MultiDexApplication
import com.github.alvarosct02.mymoviedb.data.source.local.AppDatabase
import com.github.alvarosct02.mymoviedb.data.source.local.SessionManager
import com.google.firebase.iid.FirebaseInstanceId

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        AppDatabase.init(this)
        SessionManager.init(this)

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            Log.e("FCM Token", it.token)
        }

    }
}