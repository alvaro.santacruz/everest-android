package com.github.alvarosct02.mymoviedb.features.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.github.alvarosct02.mymoviedb.utils.DialogManager

open class BaseFragment: Fragment(), LoadingView{


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupVariables()
        setupViews()
    }

    open fun setupVariables() {

    }

    open fun setupViews() {

    }

    override fun showLoading() {
        DialogManager.showProgress(requireContext())
    }

    override fun hideLoading() {
        DialogManager.hideProgress()
    }
}
