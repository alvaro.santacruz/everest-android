package com.github.alvarosct02.mymoviedb.features.splash

interface SplashView {

    fun goToHome()

    fun goToApp()

}
