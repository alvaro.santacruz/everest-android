package com.github.alvarosct02.mymoviedb.features

import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.local.IDataSourceLocal
import com.github.alvarosct02.mymoviedb.data.source.remote.IDataSourceRemote
import com.github.alvarosct02.mymoviedb.features.movies.detail.MovieDetailPresenter
import com.github.alvarosct02.mymoviedb.features.movies.detail.MovieDetailView
import com.github.alvarosct02.mymoviedb.utils.DataSourceRepositoryTest
import com.github.alvarosct02.mymoviedb.utils.RxImmediateSchedulerRule
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class MovieDetailPresenterTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    private val TITLE = "titulo"
    private val YEAR = "1999"
    private val POSTER = "url..."
    private val MOVIE_ID = "MOVIE_ID"
    private val KEYWORD = "KEYWORD"
    private val USER_ID = "USER_ID"

    @Mock
    private lateinit var mockLocal: IDataSourceLocal

    @Mock
    private lateinit var mockRemote: IDataSourceRemote

    @Mock
    private lateinit var mockRepo: DataSource

    @Mock
    private lateinit var mockPresenter: MovieDetailPresenter

    @Mock
    private lateinit var mockView: MovieDetailView

    private lateinit var movie: Movie

    @Before
    fun setUp() {
        mockRepo = DataSourceRepositoryTest(USER_ID, mockRemote, mockLocal)
        mockPresenter = MovieDetailPresenter(mockView, mockRepo)
        movie = Movie(
                title = TITLE,
                year = YEAR,
                imdbID = "",
                type = "",
                poster = POSTER,
                keywordUsed = "",
                userId = ""
        )
    }

    @Test
    fun `test view displaying accurate info`() {

        `when`(mockLocal.getSavedMovie(MOVIE_ID)).thenReturn(Single.just(movie))

        mockPresenter.getMovie(MOVIE_ID, KEYWORD)

        verify(mockView).displayTitle(TITLE)
        verify(mockView).displayPhoto(POSTER)
        verify(mockView).displayReleaseDate(YEAR)
    }

    @Test
    fun `test search for movie saved as favorite`() {
        `when`(mockLocal.getSavedMovie(MOVIE_ID)).thenReturn(Single.just(movie))

        mockPresenter.getMovie(MOVIE_ID, KEYWORD)

        verify(mockLocal).getSavedMovie(MOVIE_ID)
        verify(mockRemote, never()).getMovie(anyString())
    }

    @Test
    fun `test search for movie not saved as favorite`() {

        `when`(mockLocal.getSavedMovie(MOVIE_ID)).thenReturn(Single.error(Exception()))

        mockPresenter.getMovie(MOVIE_ID, KEYWORD)

        verify(mockLocal).getSavedMovie(MOVIE_ID)
        verify(mockRemote).getMovie(anyString())
    }

    @Test
    fun `test movie marked as favorite`() {

        `when`(mockLocal.getSavedMovie(MOVIE_ID)).thenReturn(Single.just(movie))
        `when`(mockLocal.saveMovie(movie)).thenReturn(Completable.complete())
        `when`(mockRemote.addMoviesOfUser(anyString(), anyList())).thenReturn(Completable.complete())

        mockPresenter.getMovie(MOVIE_ID, KEYWORD)
        mockPresenter.updateFavorite(true)

        verify(mockLocal).saveMovie(movie)
        Assert.assertEquals(true, movie.isSaved)
    }

    @Test
    fun `test autosync on movie marked as favorite`() {

        `when`(mockLocal.getSavedMovie(MOVIE_ID)).thenReturn(Single.just(movie))
        `when`(mockLocal.saveMovie(movie)).thenReturn(Completable.complete())
        `when`(mockRemote.addMoviesOfUser(anyString(), anyList())).thenReturn(Completable.complete())

        mockPresenter.getMovie(MOVIE_ID, KEYWORD)
        mockPresenter.updateFavorite(true)

        verify(mockRemote).addMoviesOfUser(anyString(), anyList())
    }

    @Test
    fun `test on movie marked as no longer favorite`() {

        `when`(mockLocal.getSavedMovie(MOVIE_ID)).thenReturn(Single.just(movie))
        `when`(mockLocal.updateMovie(movie)).thenReturn(Completable.complete())
        `when`(mockRemote.deleteMoviesOfUser(anyString(), anyList())).thenReturn(Completable.complete())
        `when`(mockLocal.forgetMovie(movie)).thenReturn(Completable.complete())

        mockPresenter.getMovie(MOVIE_ID, KEYWORD)
        mockPresenter.updateFavorite(false)

        verify(mockLocal).updateMovie(movie)
        Assert.assertEquals(false, movie.isSaved)
    }

    @Test
    fun `test autosync on movie marked as no longer favorite`() {

        `when`(mockLocal.getSavedMovie(MOVIE_ID)).thenReturn(Single.just(movie))
        `when`(mockLocal.updateMovie(movie)).thenReturn(Completable.complete())
        `when`(mockRemote.deleteMoviesOfUser(anyString(), anyList())).thenReturn(Completable.complete())
        `when`(mockLocal.forgetMovie(movie)).thenReturn(Completable.complete())

        mockPresenter.getMovie(MOVIE_ID, KEYWORD)
        mockPresenter.updateFavorite(false)

        verify(mockRemote).deleteMoviesOfUser(anyString(), anyList())
    }

}