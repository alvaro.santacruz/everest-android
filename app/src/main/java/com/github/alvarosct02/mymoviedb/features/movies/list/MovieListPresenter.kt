package com.github.alvarosct02.mymoviedb.features.movies.list

import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.DataSourceRepository
import com.github.alvarosct02.mymoviedb.features.base.BasePresenter
import com.github.alvarosct02.mymoviedb.utils.adt.BinarySearchTree
import com.github.alvarosct02.mymoviedb.utils.adt.DoublyLinkedList
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class MovieListPresenter(
        view: MovieListView,
        private val dataSourceRepository: DataSource = DataSourceRepository(),
        private val movieList: DoublyLinkedList<Pair<String, BinarySearchTree<Movie>>> = DoublyLinkedList()
) : BasePresenter<MovieListView>(view) {


    open fun searchMovies() {
        view.showLoading()
        disposable.add(Observable.fromIterable(dataSourceRepository.getKeywords())
                .toList()
                .flatMap { dataSourceRepository.searchMovies(it) }
                .toObservable()
                .flatMapIterable { it }
                .map { storeMovieInAdt(it) }
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideLoading()
                    if (it.isEmpty()) view.displayNoMoviesMessage() else showMovies(false)
                }, {
                    view.hideLoading()
                    view.displayNoMoviesMessage()
                })
        )
    }

    protected fun showMovies(loadedFromLocal: Boolean) {
        view.displayMovies(movieList, loadedFromLocal)
    }

    protected fun storeMovieInAdt(movie: Movie) {
        movieList.forEach {
            if (it.first == movie.keywordUsed) {
                it.second.add(movie)
                return
            }
        }

        val tree = BinarySearchTree<Movie>(comparator = { v1, v2 ->
            (v1.released ?: v1.year).compareTo(v2.released ?: v2.year)
        })
        tree.add(movie)
        movieList.addFirst(Pair(movie.keywordUsed, tree))

    }
}
