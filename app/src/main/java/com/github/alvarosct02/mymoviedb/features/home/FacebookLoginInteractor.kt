package com.github.alvarosct02.mymoviedb.features.home

import androidx.fragment.app.Fragment
import com.crashlytics.android.Crashlytics
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth

class FacebookLoginInteractor(
        private val context: Fragment,
        callbackManager: CallbackManager
) {

    private val auth = FirebaseAuth.getInstance()
    private lateinit var onSuccess: () -> Unit
    private lateinit var onFailure: (String?) -> Unit

    init {

        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                handleFacebookAccessToken(loginResult.accessToken)
            }

            override fun onCancel() {
                onFailure(null)
            }

            override fun onError(error: FacebookException) {
                onFailure(error.message)
            }
        })
    }

    private fun handleFacebookAccessToken(accessToken: AccessToken) {
        val credential = FacebookAuthProvider.getCredential(accessToken.token)

        auth.signInWithCredential(credential).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                onSuccess()
            } else {
                onFailure(task.exception?.message)
            }
        }
    }

    fun login(onSuccess: () -> Unit, onFailure: (String?) -> Unit) {
        this.onSuccess = onSuccess
        this.onFailure = onFailure
        LoginManager.getInstance().logInWithReadPermissions(context, listOf("email", "public_profile"))
    }

}
