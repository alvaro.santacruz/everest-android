package com.github.alvarosct02.mymoviedb.features.movies.detail

import com.github.alvarosct02.mymoviedb.data.models.Movie
import com.github.alvarosct02.mymoviedb.data.source.DataSource
import com.github.alvarosct02.mymoviedb.data.source.DataSourceRepository
import com.github.alvarosct02.mymoviedb.features.base.BasePresenter
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class MovieDetailPresenter(
    view: MovieDetailView,
    private val dataSourceRepository: DataSource = DataSourceRepository()
) : BasePresenter<MovieDetailView>(view) {

    private var movie: Movie = Movie()

    fun getMovie(id: String, movieKeyword: String) {
        view.showLoading()
        disposable.add(
            dataSourceRepository.getMovie(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    movie = it.apply {
                        isSaved = isSaved.and(keywordUsed == movieKeyword)
                        keywordUsed = movieKeyword
                    }
                    view.displayTitle(it.title)
                    view.displayPlot(it.plot ?: "")
                    view.displayPhoto(it.poster)
                    view.displayReleaseDate(it.year)
                    it.imdbRating?.toDoubleOrNull()?.also {rating ->
                        view.displayRating(rating, it.imdbVotes ?: "0")
                    }
                    view.displaySavedIndicator(it.isSaved)
                    view.hideLoading()
                }, {
                    view.hideLoading()
                })
        )
    }

    fun updateFavorite(isNew: Boolean) {

        val operation = if (isNew) saveMovie(movie) else forgetMovie(movie)

        disposable.add(
            operation
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        )
    }

    private fun saveMovie(movie: Movie): Completable {
        return dataSourceRepository.saveMovie(movie)
            .andThen(dataSourceRepository.addMoviesOfCurrentUser(listOf(movie)))
    }

    private fun forgetMovie(movie: Movie): Completable {
        return dataSourceRepository.forgetMovie(movie)
            .andThen(dataSourceRepository.deleteMoviesOfCurrentUser(listOf(movie))
                .andThen(dataSourceRepository.deleteMovieLocally(movie)))

    }
}
