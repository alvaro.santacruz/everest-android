package com.github.alvarosct02.mymoviedb.features.base

import io.reactivex.disposables.CompositeDisposable

open class BasePresenter<T>(
    protected val view: T
) {

    val disposable = CompositeDisposable()

    fun onDestroy() {
        disposable.dispose()
    }
}
